<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */
class Servicios extends MX_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('user');

			$this->load->config('config');
      $this->load->library('parser');
			//---base variables
			$this->base_url = base_url();
			$this->module_url = base_url() . $this->router->fetch_module() . '/';
			$this->lang->load('user/profile', $this->config->item('language'));
			$this->idu = (double) $this->session->userdata('iduser');
		  $this->user = $this->user->get_user($this->idu);

      error_reporting(E_ERROR | E_PARSE);

		}


    function index (){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $this->parser->parse('servicios', $data);

    }


}
