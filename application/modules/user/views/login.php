
<!DOCTYPE html>

<html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ESTUDIO JURIDICO - Negri, Curzi & Asociados</title>
		<!-- Meta Description -->
        <meta name="description" content="ESTUDIO JURIDICO - Negri, Curzi & Asociados">
        <meta name="keywords" content="Estudio Jurídico Derecho Penal Comercial Buenos Aires Trayectoria Curzi Negri">
        <meta name="author" content="Luciano Menez">

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='{base_url}site/assets/css/google_fonts.css' rel='stylesheet' type='text/css'>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{base_url}site/assets/css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{base_url}site/assets/css/main.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{base_url}site/assets/js/modernizr-2.6.2.min.js"></script>

    </head>

    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
          </button>
					<!-- /responsive nav button -->

					<!-- logo -->
					<!-- <h1 class="navbar-brand"> -->
						<img class="logo" src="{base_url}site/assets/img/negri250.png">
					<!-- </h1> -->
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="{base_url}">Inicio</a></li>
                        <li><a href="{base_url}">El estudio</a></li>
                        <li><a href="{base_url}servicios" class="external" >Servicios</a></li>
                        <li><a href="{base_url}profesionales" class="external">Profesionales</a></li>
                        <li><a href="{base_url}notas/indice">Notas</a></li>
                        <li><a href="{base_url}">Contacto</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->

            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->


				<section class="marine p-0 computer" style="height:100vh;">

				<div class="container">

					<div class="row justify-content-center" style="margin-top: 13.2vh;">
						<div class="col-4 " style="margin-top: 10vh;">
							<div class="title-log text-center mb-4">
								{lang loginMsg}
							</div>
							<div class="form-box" id="login-box">
									<!-- <div class="header bg-navy txt-blanco">{lang loginMsg}</div> -->
									<form id="formAuth" action="{authUrl}" method="post">
											<div class="body bg-gray">
											<!--  MSG -->
										{if {show_warn}}
													<div class="form-group alert alert-warning">
														 <button type="button" class="close" data-dismiss="alert">&times;</button>
										<strong>{msgcode}</strong>
													</div>
								{/if}
							 <!--  NAME -->
													<div class="form-group txt-blanco sinborde">
															<input type="text" name="username" class="form-control input-log " placeholder="{lang username}"/>
													</div>
											 <!--  PASS -->
													<div class="form-group sinborde">
															<input type="password" name="password" class="form-control input-log" placeholder="{lang password}"   onkeyup="
					var start = this.selectionStart;
					var end = this.selectionEnd;
					this.value = this.value.toLowerCase();
					this.setSelectionRange(start, end);
				"/>
													</div>
											 <!--  REMEMBERME -->
													<div class="form-group txt-blanco">
														 <input class="input-log" type="checkbox" value="remember-me" > {lang rememberButton}
													</div>
											</div>
											<div class="footer">
													<button type="submit" class="btn btn-log btn-block mb-2">{lang loginButton}</button>

													<p > <a class="txt-blanco" href="{module_url}recover" >
																{lang forgotPassword}
														</a></p>

											</div>
									</form>
								</div>
						</div>
					</div>

				  </div>
				</section>

				<section class="marine p-0 mobile" style="height:100vh;">


				<div class="container">

					<div class="row justify-content-center" style="margin-top: 8vh;">
						<div class="col " style="margin-top: 15vh;">
							<div class="title-log text-center mb-4">
								{lang loginMsg}
							</div>
							<div class="form-box" id="login-box">
									<!-- <div class="header bg-navy txt-blanco">{lang loginMsg}</div> -->
									<form id="formAuth" action="{authUrl}" method="post">
											<div class="body bg-gray">
											<!--  MSG -->
										{if {show_warn}}
													<div class="form-group alert alert-warning">
														 <button type="button" class="close" data-dismiss="alert">&times;</button>
										<strong>{msgcode}</strong>
													</div>
								{/if}
							 <!--  NAME -->
													<div class="form-group txt-blanco sinborde">
															<input type="text" name="username" class="form-control input-log " placeholder="{lang username}"/>
													</div>
											 <!--  PASS -->
													<div class="form-group sinborde">
															<input type="password" name="password" class="form-control input-log" placeholder="{lang password}"   onkeyup="
					var start = this.selectionStart;
					var end = this.selectionEnd;
					this.value = this.value.toLowerCase();
					this.setSelectionRange(start, end);
				"/>
													</div>
											 <!--  REMEMBERME -->
													<div class="form-group txt-blanco">
														 <input class="input-log" type="checkbox" value="remember-me" > {lang rememberButton}
													</div>
											</div>
											<div class="footer p-0">
													<button type="submit" class="btn btn-log-mobile btn-block mb-2">{lang loginButton}</button>

													<p > <a class="txt-blanco" href="{module_url}recover" >
																{lang forgotPassword}
														</a></p>

											</div>
									</form>
								</div>
						</div>
					</div>

				  </div>
				</section>





		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="{base_url}site/assets/js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="{base_url}site/assets/js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="{base_url}site/assets/js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="{base_url}site/assets/js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
		<!-- Owl Carousel -->
        <script src="{base_url}site/assets/js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="{base_url}site/assets/js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="{base_url}site/assets/js/jquery.slitslider.js"></script>
        <script src="{base_url}site/assets/js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="{base_url}site/assets/js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="{base_url}site/assets/js/main.js"></script>
    </body>
</html>
