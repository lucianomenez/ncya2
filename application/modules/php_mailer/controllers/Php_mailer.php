<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    require APPPATH.'vendor/phpmailer/phpmailer/src/Exception.php';
    require APPPATH.'vendor/phpmailer/phpmailer/src/PHPMailer.php';
    require APPPATH.'vendor/phpmailer/phpmailer/src/SMTP.php';

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Php_mailer extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('Model_api');
          $this->load->library('parser');
          $this->load->config('config');
        }


        function send_mail($msg){
          $html = $msg['body'];
          $data = '{
              "from": { "email": "'.$msg['from'].'" , "name" : "Consulta desde la WEB" },
              "reply_to": {
            		"email": "web@negriyasociados.com.ar",
            		"name": "Soporte"
            	},
              "subject": "'.$msg['subject'].'",
              "content": {"html": "'.$html.'"},
              "recipients": [{"to": {"email": "web@negriyasociados.com.ar"}}]
          }';

          $ch = curl_init('https://transactional.myperfit.com/v1/mail/send');
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Authorization: Bearer efecinco-tr-JKbzGIQgidLFlFhcIGcAeRIpKdOahbgP",
              "Content-Type: application/json"
          ));
          $result = curl_exec($ch);
          echo $result;
          //echo $this->parser->parse('perfil/exito_registro', $parseData , true, true);
          echo "Su consulta ha sido enviada!";
          header('refresh: 5; url='.$this->base_url.''); // redirect the user after 10 seconds
      }






}
