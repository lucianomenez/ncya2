<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ESTUDIO JURIDICO - Negri, Curzi & Asociados</title>
        <link rel="icon" type="image/png" href="{base_url}site/assets/img/favicon.ico"/>

		<!-- Meta Description -->
        <meta name="description" content="Estudio Jurídico - Negri Curzi y Asociados">
        <meta name="keywords" content="derecho penal economico estudio juridico capital federal">
        <meta name="author" content="Luciano Menez">

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{base_url}site/assets/css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{base_url}site/assets/css/main.css">

        <link rel="stylesheet" href="{base_url}site/assets/css/lightview.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{base_url}site/assets/js/modernizr-2.6.2.min.js"></script>

    </head>

    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
                    </button>
					<!-- /responsive nav button -->

					<!-- logo -->
					<img class="logo" src="{base_url}site/assets/img/negri250.png">
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                      <li><a href="{base_url}" class="external">Inicio</a></li>
                      <li><a href="{base_url}estudio" class="external">El estudio</a></li>
                      <li><a href="{base_url}servicios" class="external">Servicios</a></li>
                      <li><a href="{base_url}profesionales" class="external">Profesionales</a></li>
                      <li><a href="{base_url}notas/indice" class="external">Publicaciones</a></li>
                      <li><a href="{base_url}" class="external">Contacto</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->

            </div>
        </header>
<!-- about section -->
		<section id="cab-interna" class="parallax">
				<div class="overlay">
					<div class="container">
						<div class="row">

							<div class="sec-title-int text-center white wow animated fadeInDown">
								<h2>Profesionales</h2>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="profesionales">
				<div class="container">
					<div class="row">

						<div class="col-md-8 wow animated fadeInLeft">
							<h2>Dr. Carlos María Negri</h2>
						</div>

            <div class="col-md-8 wow animated fadeInLeft">
            <img  width="160" height="160" style="margin-top: 15px; margin-bottom: 15px;" class="mt-4 rounded float-left" src="{base_url}site/assets/img/negri.jpeg">
            <span>
            <a target="_blank" href="https://www.linkedin.com/in/carlos-mar%C3%ADa-negri-b675674a/?originalSubdomain=ar"><h4>Vínculo a CV Completo</h4></a>
          </span>
          </div>


						<div class="col-md-8 separador sec-title wow animated fadeInDown"></div>

						<div class="col-md-8 wow animated fadeInLeft">
								<div class="message-body practica">
                  <h5>CV Abreviado.</h5>
								      <h3>Especialista en Derecho Penal Económico</h3>
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Estudios cursados</h4>
								<div class="message-body practica">
								      +Abogado egresado con diploma de honor de la Universidad de Buenos Aires. <br>
								      +Especialización en Derecho Penal Económico. <br>
								      +Cursos en distintas universidades del país y del exterior: Universidad de San Marcos; Denver University; Florida International University; Guanabara; Universidad de Toledo, Madrid y Granada; Bruselas; La Haya y London University; Universidad de La Habana; Nueva York; Washington; Shangai y Pekin.
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Actividades docentes</h4>
								<div class="message-body practica">
								      +Director del Departamento de Derecho de la Facultad de Ciencias Económicas de la UBA. <br>
								      +Profesor Titular de la Cátedra Instituciones de Derecho Privado. Empresa y Sociedad en la Facultad de Ciencias Económicas de la UBA. <br>
								      +Profesor Titular en el posgrado en Sindicatura Concursal de la materia Derecho Penal Económico, Facultad de Ciencias Económicas de la UBA. <br>
								      +Profesor de posgrado en la Facultad de Derecho de la UBA y en la UADE. <br>
								      +Director Coordinador del Centro Economía y Delito de la Facultad de Ciencias Económicas (UBA).
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Cargos desempeñados:</h4>
                <br/>
								<h5>Justicia</h5>
								<div class="message-body practica">
								    +Secretario del Juzgado en lo Civil y Comercial No. 2 de San Isidro (1971-1975).<br>
								    +Fiscal Nacional de Primera Instancia en lo Penal Económico (1975-1978).
								</div>

								<h5>Administración pública</h5>
								<div class="message-body practica">
								      +Síndico del BCRA. (Octubre 1986/Julio 1988).<br>
								      +Interventor de Canal 13 de Buenos Aires (1985/86).<br>
								      +Presidente de Canal 11 (Dicon S.A.) Julio (1988-1989).
								</div>

								<h5>Universidades nacionales</h5>
								<div class="message-body practica">
								      +Director del Departamento de Derecho de la FCE de la UBA. <br>
								      +Director coordinador y presidente de la Comisión Delitos Societarios del Centro de Estudios Economía y Delito (FCE, UBA). Cofundador de dicho centro de estudios. <br>
								      +Miembro titular del Consejo Académico Normalizador Consultivo de la Facultad de Ciencias Económicas de la UBA.
								</div>

								<h5>Colegio público de abogados</h5>
								<div class="message-body practica">
								      +Electo miembro titular de la asamblea de delegados del Colegio Público de Abogados de la Capital Federal (2012/2016).
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Actividades docentes</h4>
								<div class="message-body practica">
								      +Director del Departamento de Derecho de la Facultad de Ciencias Económicas de la UBA. <br>
								      +Profesor Titular de la Cátedra Instituciones de Derecho Privado. Empresa y Sociedad en la Facultad de Ciencias Económicas de la UBA. <br>
								      +Profesor Titular en el posgrado en Sindicatura Concursal de la materia Derecho Penal Económico, Facultad de Ciencias Económicas de la UBA. <br>
								      +Profesor de posgrado en la Facultad de Derecho de la UBA y en la UADE. <br>
								      +Director Coordinador del Centro Economía y Delito de la Facultad de Ciencias Económicas (UBA).
							    </div>
							</div>



							<div class="col-md-8 wow animated fadeInLeft">
								<h2>Dr. Sergio Curzi -1976-</h2>

							</div>

              <div class="col-md-8 wow animated fadeInLeft">
              <img width="160" height="160" style="margin-top: 15px; margin-bottom: 15px;" class="mt-4 rounded float-left" src="{base_url}site/assets/img/rsz_curzi.jpg">
              <span>
                <a target="_blank" href="https://www.linkedin.com/in/sergio-curzi-07853a23/?originalSubdomain=ar"><h4>Vínculo a CV Completo</h4></a>
            </span>
            </div>

							 <div class="col-md-8 separador sec-title"></div>

							 <div class="col-md-8 wow animated fadeInLeft">
								<div class="message-body practica">
                      <h5>CV Abreviado.</h5>
								      <h3>Especialista en Derecho Penal y Ciencias Penales</h3>
								</div>
							</div>


							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Estudios cursados</h4>
								<div class="message-body practica">
								      +Abogado egresado de la Universidad de Buenos Aires con orientación en Derecho Penal.<br>
								      +Posgrado. Especialista en Derecho Penal y Ciencias Penales.<br>
								      +Ha realizado sendos cursos de perfeccionamiento en diversas áreas vinculadas al Derecho.<br>
								      +Ingles avanzado.
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Cargos desempeñados</h4>
                <br/>
								<div class="message-body practica">
								      +Funcionario del Poder Judicial de la Nación. Secretario de Juzgado Nacional en lo Criminal de Instrucción.<br>
								      +Previamente hubo realizado la carrera judicial ingresando “ad honorem” (1995) y luego como empleado de la Cámara de Apelaciones en lo Criminal y Correccional, desempeñándose en todos los cargos: Auxiliar, Escribiente Aux. Escribiente, Oficial, Jefe de Despacho y Prosecretario Jefe, con participación activa en la implementación de normas de gestión y calidad (ISO-9001).<br>
								      +Ha sido designado en los cargos de Gerente y Director de diversas empresas tanto nacionales, como multinacionales.
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Actividad docente y académica</h4>
								<div class="message-body practica">
								      +Profesor Adjunto de la Universidad UPFA. Carrera de Abogacía en la materia “Delitos Penales Especiales”.<br> 
								      +Socio Fundador del “Centro de Estudios de Economía y Delito” de la Facultad de Ciencias Económicas de la U.B.A.<br> 
								      +Autor de la publicación "Democratización de la Justicia" en la Revista Comisión Interna Gremial del Fuero Penal.<br>
								      +Ha sido invitado como expositor en temas como "Responsabilidad Penal Empresaria". Conferencia organizada por el Centro de Estudios e investigación Jurídica de la Empresa (CEIJE). Facultado de Ciencias Económicas de la UBA.<br>
								      +Se ha asociado a la World Compliance Association (WCA), con sede en Madrid, asociación internacional que nuclea a países de Europa y Latinoamérica en materia de compliance.
								</div>
							</div>

							<div class="col-md-8 wow animated fadeInLeft">
								<h4>Cursos, congresos y seminarios</h4>
								<div class="message-body practica">
								      +“Derecho Penal Tributario e infracciones impositivas”, dictado por el Consejo de la Magistratura de la Nación.<br>
								      +“Procedimiento Penal Tributario”, organizado por la Facultad de Ciencias Económicas de la U.B.A.<br>
								      +”Quiebra y delito”, organizado por la Facultad de Ciencias Económicas de la U.B.A. y el Centro de Estudios de Economia y Delito.<br>
								      +Estrategias en la ”Defensa Penal”, organizado por seminarios Hamumurabi.<br>
								      +“Derehcos Humanos y Justicia Penal en América Latina”, organizado por la Corte Suprema de Justicia de la Nación.<br>
								      +“Contabilidad y Tributación”, dictado por el Consejo de la Magistratura de la Nación.<br>
								      +”Seguridad Bancaria en relación a los delitos con tarjeta de crédito y débito”, organizado por la Cámara de Tarjetas de Crédito y Compra “ATACYC”.<br>
								      +“El protagonismo del Pueblo en el Juzgamiento de los delitos y juicio por jurado”, dictado en el Honorable Senado de la Nación.<br>
								      +“Técnicas de redacción e interpretación jurídica de textos”, dictado por el Consejo de la Magistratura de la Nación.<br>
								      +“El control de la actividad aseguradora, reaseguradora e intermediación: órdenes jurídicos, técnicos y económicos”, organizado por la Superintendencia de Seguros de la Nación.<br>
								      +“Congreso de Derecho Penal y Garantias”, dictado por el Dpto. de Derecho Penal de la Facultad de Derecho de la U.B.A.<br>
								      +“Reforma de Código Procesal, organizado por la Comision Interna del Fuero Penal del Sindicato. Union de empleados del Poder Judicial de la Nación.<br>
								      +“Blaqueo, Moratoria y regulación de empleo”, organizado por editoria ERREPAR.<br>
								      +Inversiones Bursátiles. Dictado por la Fundación Bolsa de Comercio de Buenos Aires.<br>
								      +“Ley de Lavado”, organizado por el Centro de Economia y Delito y la Facultad de Derecho de la U.B.A. Compliance para especialistas nacional e internacional.
								</div>
							</div>



					</div>
				</div>
			</section>




      <footer id="footer">
  			<div class="container">
  				<div class="row text-center">
  					<div class="footer-content">

  						<!-- <form action="#" method="post" class="subscribe-form wow animated fadeInUp">
  							<div class="input-field">
  								<input type="email" class="subscribe form-control" placeholder="Enter Your Email...">
  								<button type="submit" class="submit-icon">
  									<i class="fa fa-paper-plane fa-lg"></i>
  								</button>
  							</div>
  						</form>
  						<div class="footer-social">
  							<ul>
  								<li class="wow animated zoomIn"><a href="#"><i class="fa fa-thumbs-up fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-skype fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="0.9s"><a href="#"><i class="fa fa-dribbble fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="1.2s"><a href="#"><i class="fa fa-youtube fa-3x"></i></a></li>
  							</ul>
  						</div> -->

  						<p>Copyright &copy; 2020 - Estudio Negri, Curzi y Asociados Diseñado e Implementado por Luciano Menez y Victoria Cariac</p>
  					</div>
  				</div>
  			</div>
  		</footer>

		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="{base_url}site/assets/js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="{base_url}site/assets/js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="{base_url}site/assets/js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="{base_url}site/assets/js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Owl Carousel -->
        <script src="{base_url}site/assets/js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="{base_url}site/assets/js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="{base_url}site/assets/js/jquery.slitslider.js"></script>
        <script src="{base_url}site/assets/js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="{base_url}site/assets/js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="{base_url}site/assets/js/main.js"></script>
    </body>
</html>
