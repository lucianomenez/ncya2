
     {productos}
              <div class="col-xs-12 col-sm-6 col-lg-4 p-4 productos-item">
                <!-- nueva o clasicos -->
                  <div class="producto marco neutral d-flex">
                      <img src="{image}" class="producto front link" slug="{base_url}{slug}">
                      <img src="{image_back}" class="producto back link" slug="{base_url}{slug}" style="display: none;">
                  </div>
                   <div class="title-prod pl-1 pr-1 mt-2 computer">{title}</div>
                   <div class="title-prod-mobile pl-1 pr-1 mt-2 mobile">{title}</div>
              </div>
            {/productos}
