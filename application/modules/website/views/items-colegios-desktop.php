            {colegios}
              <div class="col-xs-12 col-sm-6 col-lg-4 p-4 colegios-item" >
                <div class="colegio link p-2" slug="{base_url}{slug}">
                <!-- nueva o clasicos -->
                  <div class="imagen-colegio" style="background-image: url('{card_image}'); height: 200px;"></div>
                  <div class="info-colegio bold mt-2">
                    {title} <small>{ano}</small>
                  </div>
                  <div class="info-colegio-s">
                    {city}
                  </div>
                </div>
              </div>
            {/colegios}

