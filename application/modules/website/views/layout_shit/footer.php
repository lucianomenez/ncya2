<footer id="footer">
  <div class="container">
    <div class="row text-center">
      <div class="footer-content">

        <!-- <form action="#" method="post" class="subscribe-form wow animated fadeInUp">
          <div class="input-field">
            <input type="email" class="subscribe form-control" placeholder="Enter Your Email...">
            <button type="submit" class="submit-icon">
              <i class="fa fa-paper-plane fa-lg"></i>
            </button>
          </div>
        </form>
        <div class="footer-social">
          <ul>
            <li class="wow animated zoomIn"><a href="#"><i class="fa fa-thumbs-up fa-3x"></i></a></li>
            <li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-3x"></i></a></li>
            <li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-skype fa-3x"></i></a></li>
            <li class="wow animated zoomIn" data-wow-delay="0.9s"><a href="#"><i class="fa fa-dribbble fa-3x"></i></a></li>
            <li class="wow animated zoomIn" data-wow-delay="1.2s"><a href="#"><i class="fa fa-youtube fa-3x"></i></a></li>
          </ul>
        </div> -->

        <p>Copyright &copy; 2020 - Estudio Negri, Curzi y Asociados Diseñado e Implementado por Luciano Menez y Victoria Cariac</p>
      </div>
    </div>
  </div>
</footer>

<!-- Essential jQuery Plugins
================================================== -->
<!-- Main jQuery -->
    <script src="{module_url}assets/js/jquery-1.11.1.min.js"></script>
<!-- Twitter Bootstrap -->
    <script src="{module_url}assets/js/bootstrap.min.js"></script>
<!-- Single Page Nav -->
    <script src="{module_url}assets/js/jquery.singlePageNav.min.js"></script>
<!-- jquery.fancybox.pack -->
    <script src="{module_url}assets/js/jquery.fancybox.pack.js"></script>
<!-- Google Map API -->
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
<!-- Owl Carousel -->
    <script src="{module_url}assets/js/owl.carousel.min.js"></script>
    <!-- jquery easing -->
    <script src="{module_url}assets/js/jquery.easing.min.js"></script>
    <!-- Fullscreen slider -->
    <script src="{module_url}assets/js/jquery.slitslider.js"></script>
    <script src="{module_url}assets/js/jquery.ba-cond.min.js"></script>
<!-- onscroll animation -->
    <script src="{module_url}assets/js/wow.min.js"></script>
<!-- Custom Functions -->
    <script src="{module_url}assets/js/main.js"></script>
