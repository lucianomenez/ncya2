<html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ESTUDIO JURIDICO - Negri, Curzi & Asociados</title>
		<!-- Meta Description -->
        <meta name="description" content="Blue One Page Creative HTML5 Template">
        <meta name="keywords" content="one page, single page, onepage, responsive, parallax, creative, business, html5, css3, css3 animation">
        <meta name="author" content="Luciano Menez">

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='{module_url}assets/css/google_fonts.css' rel='stylesheet' type='text/css'>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{module_url}assets/css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{module_url}assets/css/main.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{module_url}assets/js/modernizr-2.6.2.min.js"></script>

    </head>
