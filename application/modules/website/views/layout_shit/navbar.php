  <body id="body">
  <!-- preloader -->
      <div id="preloader">
              <div class="loder-box">
                <div class="battery"></div>
              </div>
      </div>
  <!-- end preloader -->
      <!--
      Fixed Navigation
      ==================================== -->
      <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
          <div class="container">
              <div class="navbar-header">
                  <!-- responsive nav button -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
                  </button>
        <!-- /responsive nav button -->

        <!-- logo -->
        <!-- <h1 class="navbar-brand"> -->
          <img class="logo" src="{module_url}assets/img/logo250.png">
        <!-- </h1> -->
        <!-- /logo -->
              </div>

      <!-- main nav -->
              <nav class="collapse navbar-collapse navbar-right" role="navigation">
                  <ul id="nav" class="nav navbar-nav">
                      <li><a href="#body">Inicio</a></li>
                      <li><a href="#about">El estudio</a></li>
                      <li><a href="areas.html" class="external" >Áreas de práctica</a></li>
                      <li><a href="profesionales.html" class="external">Profesionales</a></li>
                      <li><a href="#portfolio">Notas</a></li>
                      <li><a href="#contact">Contacto</a></li>
                  </ul>
              </nav>
      <!-- /main nav -->

          </div>
      </header>
