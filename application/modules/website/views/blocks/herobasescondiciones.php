
  <header >
        <div style="height:3.5rem; background: linear-gradient(135deg, {gradient_from} 0%, {gradient_to} 100%);">
              <div class="container" style="line-height:3.5rem;font-size: 2.1rem;color: {color};font-weight: bold;">{title1}</div>
        </div>
        <div class="container mt-2" style="font-size: 1.7rem;font-weight: bold;">
            {title2}
        </div>
  </header>
