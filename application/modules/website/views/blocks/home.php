<input type="hidden" id="page" value="home">
<input type="hidden" id="image" value="{image}">

<style type="text/css">
          body {
            overflow-y: hidden!important;
            overflow-x: hidden!important;
          }
          .slick-active, .slick-slide{
            opacity: unset!important;
          }
</style>
<div id="top">
<div class="computer" id="homePageComputer">
  <!-- Header -->
  <div class="section">


  <div class="container" style="height:20vh; background:#fff;">

  </div>
  <header class="masthead " style="height:80vh;background: linear-gradient(135deg, rgba({gradient_from_hero}) 0%, rgba({gradient_to_hero}) 100%);">
  <div class="xx-large text-uppercase">
    {title_xx}
      <!-- {title_back} -->
  </div>
  <div class="container" style="z-index:5!important;">
    <div class="parallax-window" data-parallax="scroll" data-speed="1">
        <div class="overlay-window-vertical" data-scroll-speed="">
          <img src="{image}" alt="" class="img-principal">
        </div>
        <div class="header title front mt-3">
          <div class="overlay-window-horizontal" data-scroll-speed="100">
             <div class="text-uppercase">
              <p class="inner">{title1}</p>
              <p class="inner bold">{title2}</p>
            </div>
          </div>
        </div>
    </div>
  </div>
</header>
  </div>
<!-- <header class="masthead section" style="height:100vh; background:#fff;margin-top:">
    <div class="container" style="z-index:5!important;">
      <div class="parallax-window" data-parallax="scroll" data-speed="1">
          <div class="overlay-window-vertical" data-scroll-speed="">
            <div class="img-principal" style="background: url('{image}');"></div>
          </div>
          <div class="header title front mt-3">
            <div class="overlay-window-horizontal" data-scroll-speed="100">
               <div class="text-uppercase">
                <p class="inner">{title1}</p>
                <p class="inner bold">{title2}</p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </header>  -->

  <a class="no-style" href="{base_url}productos"><button class="btn btn-principal sinborde">
    <div class="row align-items-center">
      <div class="col text-left">
        <div class="btn-title">{btn_title_top}</div>
        <div  class="btn-txt">{btn_title_bottom}</div>
      </div>
      <div class="col">
        <i class="fas fa-caret-right"></i>
      </div>
    </div>
  </button>
</a>

  <!-- <section class="section p-0">
    <div class="container-fluid">
      <div class="row" style="height:70vh;">
        <div class="col-4">

        </div>
        <div class="col-4 " >
          <input type="hidden" id="seccion1" value="{seccion1}">
          <div style="overflow: hidden;height: 70vh;position: relative;">
            <div class="container secciones hover-effect" style="background:url('{seccion1}');">

            </div>
          </div>
          <a href="{base_url}productos" class=""><button class="btn-secciones sinborde" style="z-index: 2!important;">Productos</button></a>
        </div>

        <div class="col-4">
           <input type="hidden" id="seccion2" value="{seccion2}">
           <div style="overflow: hidden;height: 70vh;position: relative;">
          <div class="container secciones hover-effect" style="background:url('{seccion2}');">

          </div>
          </div>
          <a href="{base_url}colegios" class=""><button class="btn-secciones sinborde" style="z-index: 2!important;">Colegios</button></a>
        </div>
      </div>
    </div>

  </section> -->
  <section class="section p-0">
    <div class="container">
      <div class="row justify-content-center" style="height:20vh; margin-top:12vh;">
          <div class="col text-center">
            <div class="container p-4">
              <div class="banner-dato">
                {1_bajada}
              </div>
              <input type="hidden" id="banner1_dato" value="{1_dato}" class="banner-number-input">
              <div class="banner-number"  id="banner1_datocount">
                0
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="container p-4">

              <div class="banner-dato">
                {2_bajada}
              </div>
                 <input type="hidden" id="banner2_dato" value="{2_dato}" class="banner-number-input">
              <div class="banner-number" id="banner2_datocount">
                    0
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="container p-4">
              <div class="banner-dato">
                {3_bajada}
              </div>
                 <input type="hidden" id="banner3_dato" value="{3_dato}" class="banner-number-input">
              <div class="banner-number" id="banner3_datocount">
                  0
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="container p-4">
              <div class="banner-dato">
                {4_bajada}
              </div>
               <input type="hidden" id="banner4_dato" value="{4_dato}" class="banner-number-input">
              <div class="banner-number" id="banner4_datocount">
                    0
              </div>
            </div>
          </div>
        </div>
      <div class="row" style="height:80vh; margin-top:12vh;">
        <div class="col-5">
          <div class="title-insti mb-4">
            {title_institucional}
          </div>
          <p>
            {txt_institucional}
          </p>
          <div class="sub-title-insti" style="margin-top:8vh;">
            {1_subtitle_institucional}
          </div>
          <div class="sub-title-insti" style="font-size:12vh;">
            {2_subtitle_institucional}
          </div>
        </div>
        <div class="col-7">
          <div class="marco-insti" style="background: linear-gradient(135deg, rgba({gradient_from_insti}) 0%, rgba({gradient_to_insti}) 100%)"></div>
            <div class="figura-insti" style="background:url('{figura_insti}');"></div>
          <!-- <section class="regular slider p-0">
              <div>
                <div class="figura-insti" style="background:url('{image}');"></div>
              </div>
              <div>
                <div class="figura-insti" style="background:url('{image}');"></div>
              </div>
              <div>
                <div class="figura-insti" style="background:url('{image}');"></div>
              </div>
          </section> -->

        </div>
      </div>
    </div>

  </section>

  <!-- Institucional -->

<section class="pt-0 pb-0 section" style="z-index:4!important; background:#fff;overflow:hidden;position: relative;">
    <video poster="{prev_video}" playsinline="playsinline" loop="loop" class="institucional lazyload" id="video_institucional" preload="none" >
     <!--  <source src="{video_institucional}" type="video/mp4" style="width: 100vw;"> -->
      <source data-src="{base_url}images/web/video-institucional.mp4" type="video/mp4" style="width: 100vw;">
    </video>

    <div class="container" style="margin-bottom: 3.5rem!important;">
      <div class="row d-flex justify-content-center mt-4"style="z-index:4;">
        <button class="btn-video btn-{color_txt} play sinborde" style="z-index: 2!important;">Ver video</button>
        <button class="btn-video btn-{color_txt} pause sinborde" style="z-index: 2!important;display:none;">Pausar video</button>
      </div>
    </div>
</section>


<!-- Beneficios -->
<section class="page-section pt-0 pb-0 section" id="beneficios" style="background:#fff;position: relative;">
  <div class="container aquamarine" style="width:50vw; height:100vh;z-index:0; position:absolute; top:0; right:0;"></div>
  <div class="container">
    <div class="row justify-content-center align-items-center" style="margin-top:5vh;">
      <div class="col-4">
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="rewards">
         <!--  <source src="{video_rewards}" type="video/mp4"> -->
            <source src="{base_url}images/web/video-rewards.mp4" type="video/mp4">
        </video>
      </div>
      <div class="col-5 neutral p-4" style="width:10vw;">
        <div class="container" style="width: 100%;position: absolute; top: -12vh; left: 3vw;">
          <div class="banner-title-right" style="color:#05192C">
            {title_rewards1}
          </div>
          <div class="banner-title-right" style="color:#05192C">
            {title_rewards2}
          </div>
        </div>
        <div class="container mb-2 ">
          <div class="banner-txt-right">
            {txt_rewards}
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<!-- Compromiso -->
<section class="page-section pt-0 pb-0 section" id="beneficios" style="background:#fff;position: relative;">
  <div class="container aquamarine" style="width:50vw; height:100vh;z-index:0; position:absolute; top:0; left:0;"></div>
  <div class="container">
    <div class="row justify-content-center align-items-center" style="margin-top:5vh;">
      <div class="col-5 p-0">

        <div class="container" style="width: 90%;position: absolute; top: -12vh;left: -2.5vw;">
          <div class="banner-title-left" style="color:#05192C;">
            {title_compromiso}
          </div>
        </div>
        <div class=" neutral p-4 mb-2" style="margin-left: -3rem;">
          <div class="banner-txt-left">
          {txt_compromiso}
          </div>
        </div>

      </div>
      <div class="col-4 pl-0">
              <div class="img-compromiso" style=" background: url({img_compromiso})"></div>

        <!-- <section class="regular slider p-0">
            <div>
              <div class="img-compromiso" style=" background: url({prev_video})"></div>
            </div>
            <div>
              <div class="img-compromiso" style=" background: url({prev_video})"></div>
            </div>
            <div>
              <div class="img-compromiso" style=" background: url({prev_video})"></div>
            </div>
        </section> -->

      </div>

    </div>
  </div>
</section>


<section class="turquesa fp-auto-height pt-0 section">
  <div class="container" style="margin-top:10vh!important;">
    <div class="row mt-5  text-left">
      <div class="container">
        <div class="logos title mb-4 mt-5">
        {title_caminos}
        </div>
      </div>


      <section class="regular slider p-0">

         {caminos}
        <div>
          <img class="alianzas logos lazyload" data-src="{url}">
        </div>

        {/caminos}

      </section>

    </div>
    <div class="row  text-left" style="margin-top:10vh;">
      <div class="container">
        <div class="logos title mb-4">
        {title_alianzas}
        </div>
      </div>

    <section class="regular slider p-0">

        {alianzas}
            <div class="slick-active">
              <img class="alianzas logos lazyload" data-src="{url}">
            </div>
        {/alianzas}

      </section>

    </div>

  </div>
</section>

<section class="pt-0 pb-0 section marine computer">
  <div class="container" style="width:80%!important;">
    <div class="row align-items-center justify-content-center">
      <div class="col-5">
        <div class="container">
          <div class="puntos-title mb-4">
            {title_puntos}
          </div>
        </div>
      </div>
      <div class="col-6">
        <div class="container text-left">
          {items}
           <span class="puntos-name mr-4">{name_punto}</span>
          {/items}
        </div>
      </div>
    </div>
    <div class="row align-items-center justify-content-center" style="margin-top:10%;">
      <div class="container txt-puntos text-center">
        {txt_puntos}
      </div>
    </div>
  </div>
</section>
<!-- {guion} -->
<section class="section fp-auto-height marine" style="padding-top:0rem; padding-bottom:3rem;">
  <div class="container align-items-center">
    <div class="row align-items-end" >
      <div class="col-5">
        <div class="container">
          <div class="footer-title-2" style="color:#2DF19C;">
            envíos <b>gratis</b> a<br>donde <b>vos</b> estés
          </div>
        </div>
        <div class="container mb-4">
          <div class="footer-title mb-3" style="color:#fff;">
            contactanos
          </div>

          <div class="footer-txt"><i class="fab fa-whatsapp fa-1x mr-3 fff"></i>+ 54911 5835 2669</div>

          <div class="footer-txt mt-4"><i class="far fa-envelope fa-1x mr-3 fff"></i> info@followme.com.ar</div>
        </div>
        <div class="container">
          <div class="footer-title mr-4 mb-3" style="color:#fff;">
            seguinos
          </div>
          <a class="footer-link p-0" href="https://www.facebook.com/followmegrads" target="_blank"><img class="icon-redes mr-4 filtro" src="{base_url}images/web/facebook.png"></a>
          <a class="footer-link p-0" href="https://www.instagram.com/Followmebuzosycamperas/" target="_blank"><img class="icon-redes mr-4 filtro" src="{base_url}images/web/instagram.png"></a>
          <!-- <a class="footer-link p-0" href="" target="_blank"><img class="icon-redes mr-4 filtro" src="{base_url}images/web/pinterest.png"></a>
          <a class="footer-link p-0" href=""><img class="icon-redes mr-4 filtro" src="{base_url}images/web/tiktok.png"></a> -->
        </div>
      </div>

      <div class="col-7" style="position:relative;">
         <span id="submit_computer_succes" class="" style="display: none;color:#2DF19C;position: absolute;top:-260px;">Mensaje Eviando<br>Nos comunicaremos a la brevedad</span>

        <form role="form" id="form_enviar"   action="" class="form-contact col-xs-12 col-sm-12 col-md-12" style="margin: 0 auto;">
          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="nombre" placeholder="Nombre de Contacto *" name="nombre" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="colegio" placeholder="Colegio *" name="colegio" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="localidad" placeholder="Localidad *" name="localidad" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="telefono" placeholder="Teléfono *" name="telefono" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="email" placeholder="Email*" name="email" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <select class="form-control input-modal custom-select" name="nivel" id="nivel" required="">
                  <option value="" selected="" disabled="">Nivel escolar *</option>
                  <option>Primaria</option>
                  <option>Secundaria</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group ">
                <select class="form-control input-modal custom-select" name="promocion" id="promocion" required="">
                  <option value="" selected="" disabled="">Promoción *</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12" >
              <div class="form-group">
                <textarea class="form-control input-modal" id="consulta" name="consulta" placeholder="Consulta*" rows="4" required=""></textarea>
              </div>

                <div class="g-recaptcha" data-sitekey="6Le7YuQUAAAAAMIjimpp-eb60ZbkoF7jhFaQoQ0f"></div>

                <div class="form-group" style="position: relative;">
                  <span id="recaptcha_msg" class="" style="display: none;color:red;position: absolute;top: 0;">Marcá no soy un robot</span>
                  <input id="submit_computer" type="submit" name="submit" value="Enviar" class="btn pull-right btn-modal mt-4">
                </div>

            </div>
          </div>
        </form>
      </div>
      </div>
    </div>
</section>

</div>

<!---------------------------------------------------------------- MOBILE --------------------------------------------------------------->

<div class="mobile " id="homePageMobile" style=" touch-action: none; transform: translate3d(0px, 0px, 0px); transition: all 700ms ease 0s;">
  <!-- Header -->

  <header class="masthead section"style="position: relative; min-height: 100%;background: linear-gradient(135deg, rgba({gradient_from_hero}) 0%, rgba({gradient_to_hero}) 100%);">
        <img class="img-principal-mobile" src="{image_mobile}" />
    <div class="header-title-mobile">
      {title1} {title2}
    </div>
  </header>

  <!-- <button class="btn btn-principal-mobile sinborde">
    <a class="no-style" href="{base_url}productos">Nueva temporada<i class="fas fa-caret-right ml-5"></i></a>
  </button> -->

  <!-- Institucional -->


  <section class="pt-0 pb-0 section text-center" style="height:100%;">

        <div class="container mb-4" style="margin-top:4rem;">
          <div class="banner-dato-mobile" >
            {1_bajada}
          </div>
          <div class="banner-number-mobile" id="banner1_datocountMobile">
            0
          </div>
        </div>
        <div class="container mb-4">
          <div class="banner-dato-mobile">
            {2_bajada}
          </div>
          <div class="banner-number-mobile" id="banner2_datocountMobile">
            0
          </div>
        </div>
          <div class="container mb-4">
            <div class="banner-dato-mobile">
              {3_bajada}
            </div>
            <div class="banner-number-mobile" id="banner3_datocountMobile">
              0
            </div>
          </div>
        <div class="container mb-4">
          <div class="banner-dato-mobile">
            {4_bajada}
          </div>
          <div class="banner-number-mobile" id="banner4_datocountMobile">
            0
          </div>
        </div>
  </section>

  <section class="pt-4 pb-0 section text-center" style="height:100%;">
    <div class="container text-center">
          <div class="title-insti-mobile mb-4">
            {title_institucional}
          </div>
          <p>
            {txt_institucional}
          </p>
  </section>

  <section class="pt-0 pb-0 section text-center" style="height:100%;">

    <div class="container pl-0 pr-0" style="height:50%; position:absolute; top:0; padding-top:10vh; max-width:100%!important;">
      <div class="sub-title-insti mt-4">
        {1_subtitle_institucional}
      </div>
      <div class="sub-title-insti mb-4" style="font-size:12vh;">
        {2_subtitle_institucional}
      </div>
    </div>

    <div class="container p-0" style="height:50%;position:absolute; bottom:0;  max-width:100%!important;">
      <div class="marco-insti-mobile" style="background: linear-gradient(135deg, rgba({gradient_from_insti}) 0%, rgba({gradient_to_insti}) 100%)"></div>
      <img class="figura-insti-mobile" src="{figura_insti}" />
    </div>


</section>

<section class="pt-0 pb-0 section bloque-video-mobile" style="height: 100%; width: 100%;">
     <video poster="{prev_video_mobile}" playsinline="playsinline" loop="loop" id="video_institucionalMobile" class="institucional-mobile lazyload" >
        <source data-src="{base_url}images/web/video-institucional-mobile.mp4" type="video/mp4" >
        <source data-src="{base_url}images/web/video-institucional-mobile-webm.webm" type="video/webm">
      </video>
 <div class="container" style="margin-bottom: 3.5rem!important;">
        <div class="row d-flex justify-content-center mt-4"style="z-index:4;">
          <button class="btn-video btn-video-mobile btn-{color_txt} play sinborde mobile" style="z-index: 2!important;">Ver video</button>
          <button class="btn-video btn-video-mobile btn-{color_txt} pause sinborde mobile" style="z-index: 2!important;display:none;">Pausar video</button>
        </div>
      </div>
  </section>



<section class="page-section pt-0 pb-0 section bloque-video-mobile" style="height:100vh;">
    <div class="container">
      <div class="row justify-content-center align-items-center " style="margin-top:14vh;">
          <div class="container text-center">
            <div class="banner-title-mobile" style="color: #2DF19C;">
              {title_rewards1}
            </div>
            <div class="banner-title-mobile mb-2" style="color: #2DF19C;">
              {title_rewards2}
            </div>
            <div class="banner-txt-mobile">
            {txt_rewards}
          </div>
          <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="rewards-mobile lazyload">
              <source src="{base_url}images/web/video-rewards.mp4" type="video/mp4">
              <source src="{base_url}images/web/video-rewards-webm.webm" type="video/webm">
          </video>
      </div>
    </div>
  </div>
  </section>

  <section class="page-section pt-0 pb-0 section" style="height:100vh;">
    <div class="container">
      <div class="row justify-content-center align-items-center " style="margin-top:14vh;">
          <div class="container text-center">
            <div class="banner-title-mobile mb-2"  style="color: #00b9e3;">
              {title_compromiso}
            </div>
            <div class="banner-txt-mobile mt-3 mb-3">
            {txt_compromiso}
          </div>
          <div class="container">
              <div class="img-compromiso-mobile" style=" background: url({img_compromiso})"></div>
          </div>
      </div>
    </div>
  </section>

<section class="turquesa pt-0 section">
  <div class="container" style="margin-top:10vh; margin-bottom:10vh;">

  <div class="row justify-content-center text-center" style="margin-top:5vh;">
      <div class="container">
        <div class="logos-title-mobile mb-4">
        {title_caminos}
        </div>
      </div>

      <section class="slider p-0">
         {caminos}
        <div>
          <img class="alianzas logos-mobile lazyload" data-src="{url}">
        </div>
        {/caminos}
      </section>

    </div>

    <div class="row mt-5 justify-content-center text-center">
      <div class="container">
        <div class="logos-title-mobile mb-4">
        {title_alianzas}
        </div>
      </div>

    <section class="slider p-0">
        {alianzas}
            <div>
              <img class="alianzas logos-mobile lazyload" data-src="{url}">
            </div>
        {/alianzas}
      </section>

    </div>


  </div>
</section>
<section class="pt-0 pb-0 section marine mobile">

    <div class="container">
          <div class="container">
            <div class="puntos-title-mobile mb-2 mt-4">
              {title_puntos}
            </div>
          </div>
          <div class="container text-left mt-4">
            {items}
              <span class="puntos-name mr-3">{name_punto}</span>
            {/items}
          </div>
          <div class="container txt-puntos-mobile mt-4">
              {txt_puntos}
          </div>
    </div>
  </section>

<section class="marine section fp-auto-height" style=" padding-bottom:3rem;">

    <div class="container align-items-center justify-content-center text-center">
          <div class="btn-footer mt-3" id="mensaje-modal">ENVIANOS TUS COMENTARIOS</div>
          <div class="footer-txt-m mt-4" style="color:#fff!important;"><i class="fab fa-whatsapp fa-1x mr-2 fff"></i>+ 54911 5835 2669</div>
          <div class="footer-txt-m mt-4" style="color:#fff;"><i class="far fa-envelope fa-1x mr-2 fff"></i>info@followme.com.ar</div>
          <div class="footer-title-m mt-4" style="color:#fff;">
            seguinos
          </div>
          <div class="mt-2">
            <a class="footer-link p-0" href="https://www.facebook.com/followmegrads" target="_blank"><img class="icon-redes filtro" src="{base_url}images/web/facebook.png"></a>
            <a class="footer-link p-0" href="https://www.instagram.com/Followmebuzosycamperas/" target="_blank"><img class="icon-redes ml-2 filtro" src="{base_url}images/web/instagram.png"></a>
            <!-- <a class="footer-link p-0" href="" target="_blank"><img class="icon-redes ml-2 filtro" src="{base_url}images/web/pinterest.png"></a> -->
          </div>
      </div>
</section>
</div>
<!-- <section class="marine pb-4 pt-1 section fp-auto-height">
      <div class="container mt-4" style="position: relative;">
        <span id="submit_computer_succes" class="" style="display: none;color:#2DF19C;position: absolute;top:-260px;">Mensaje Eviando<br>Nos comunicaremos a la brevedad</span>
        <form role="form"  id="form_enviar_mobile" onsubmit="enviar_mobile();return false;" class="form-contact col-xs-12 col-sm-7 col-md-8" style="margin: 0 auto;">
          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                <input type="text" class="form-control input-modal-mobile mb-1" id="nombre" placeholder="Nombre de Contacto *" name="nombre" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal-mobile mb-1" id="colegio" placeholder="Colegio *" name="colegio" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal-mobile mb-1" id="localidad" placeholder="Localidad *" name="localidad" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal-mobile mb-1" id="telefono" placeholder="Teléfono *" name="telefono" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal-mobile mb-1" id="email" placeholder="Email*" name="email" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <select class="form-control input-modal-mobile mb-1 custom-select" name="nivel" id="nivel" required="">
                  <option value="" selected="" disabled="">Nivel escolar *</option>
                  <option>Primaria</option>
                  <option>Secundaria</option>
                </select>
              </div>
            </div>
          <div class="col-xs-12 col-sm-6">
              <div class="form-group ">
                <select class="form-control input-modal-mobile mb-1 custom-select" name="promocion" id="promocion" required="" >
                  <option value="" selected="" disabled="">Promoción *</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                <textarea class="form-control input-modal-mobile" id="consulta" name="consulta" placeholder="Consulta*" rows="2" required=""></textarea>
              </div>
              <div class="form-group" style="margin-top:.5rem; margin-bottom:0;">
                <input id="submit_mobile" type="submit" name="submit" value="Enviar" class="btn pull-right btn-modal-mobile">
              </div>
            </div>
          </div>
        </form>
      </div>
</section> -->

</div>
  <a href="#top" class="back-to-top" style="display: inline;"><i class="fa fa-chevron-up"></i></a>
  <script src="{base_url}website/assets/js/enviar.js?{rand}"></script>


  <script src="{base_url}website/assets/js/countUp.min.js" type="module"></script>

  <script src="{base_url}website/assets/js/countdown.js" type="module"></script>
