<div class="computer">
  <div class="container" style="height:20vh; background:#fff;">

  </div>
  <header class="masthead" style="height:60vh;background: linear-gradient(135deg, rgba({gradient_from}) 0%, rgba({gradient_to}) 100%);">
  <div class="xx-large">
      {title_back}
  </div>
  <div class="container">
        <img data-pin-nopin="false" src="{image}" alt="" class="figura-productos">
        <div class="header title-productos-top text-uppercase">
              {title1}
        </div>
        <div class="header title-productos text-uppercase">
            {title2}
        </div>
  </div>
</header>

</div>

<div class="mobile">
<header class="masthead" style="height:60vh;background: linear-gradient(135deg, rgba({gradient_from}) 0%, rgba({gradient_to}) 100%);margin-bottom: 1rem;position: relative;z-index: 1;">
  <div class="container text-center" style="z-index:5!important;overflow: hidden;position: relative;height: 100%; max-width: 576px!important;">
        <img data-pin-nopin="false" src="{image}" alt="" class="figura-productos-mobile">
        <div class="header title-productos-top-mobile text-uppercase">
              {title1}
        </div>
        <div class="header title-productos-mobile text-uppercase">
           {title2}
        </div>
  </div>
</header>

</div>
