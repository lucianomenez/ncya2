<input type="hidden" id="page" value="sucursales">
<input type="hidden" id="base_url" value="{base_url}">

<div class="computer">
  <div class="container" style="height:20vh; background:#fff;">

  </div>
  <header class="masthead" style="height:60vh;background: linear-gradient(135deg, rgba({gradient_from}) 0%, rgba({gradient_to}) 100%);">
<div class="container" style="overflow:hidden;">
  <div class="xx-large" style="margin-top: 1rem!important;">
      {title_back}

  </div>
</div>

  <div class="container">
        <img src="{image}" alt="" class="figura-locales">
        <div class="header title-locales-top text-uppercase">
              {title1}
        </div>
        <div class="header title-locales text-uppercase">
            {title2}
        </div>
  </div>
</header>

</div>

<div class="mobile">
<header class="masthead" style="height:60vh;background: linear-gradient(135deg, rgba({gradient_from}) 0%, rgba({gradient_to}) 100%);margin-bottom: 1rem;position: relative;z-index: 1;">
  <div class="container text-center" style="z-index:5!important;overflow: hidden;position: relative;height: 100%;" >
        <img src="{image}" alt="" class="figura-productos-mobile">
        <div class="header title-productos-top-mobile text-uppercase">
              {title1}
        </div>
        <div class="header title-productos-mobile text-uppercase">
           {title2}
        </div>
  </div>
</header>

</div>

<div id="data_sucursales_html" style="margin-top: 3rem;">

    <section class="page-section p-0 row justify-content-center m-0" style="">

      <div class="container mt-4 mb-5">
          <div class="row contenedor_sucursales " id="contenedor_sucursales" style="">
            {sucursales}
              <div class="col-xs-12 col-sm-6 col-lg-4 sucursales-item" style="padding-bottom:4rem;">
                <div class="container marco-sucu">
                  <div class="row justify-content-center " >
                    <div class="title-sucu">{title}</div>
                  </div>
                  <div class="container justify-content-center align-items-center" style="height:81%; margin-top:1rem;">

                    {address}
                    {phone}
                    {txt_alternativo}
                  </div>
                  <div class="row" style="">
                      {como_llegar}
                      {sucursal_whatsapp}
                  </div>
                </div>
              </div>
            {/sucursales}
          </div>

      </div>
    </section>

</div>
