<input type="hidden" id="page" value="colegios">
<input type="hidden" id="base_url" value="{base_url}">
<input type="hidden" id="slug" value="{slug}">

<style type="text/css">
input:focus, input.form-control:focus {
    outline:none !important;
    outline-width: 0 !important;
    box-shadow: none;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
}
</style>
<div id="data_colegios_html" style="margin-top: 3rem;">

    <section class="page-section p-0 row justify-content-center" style="">
        <div class="container" style="position: absolute; z-index: 1;">
            <form class="form-inline justify-content-center" style="">
              <input class="form-control mr-2 busqueda-colegios" type="search" placeholder="Buscar por colegio, lugar o año" aria-label="Search" id="filtro_colegio">
              <div style="cursor: pointer;" class="buscar_colegios"><i class="fas fa-search"></i></div>
            </form>
        </div>
      <div class="container mt-5">
          <div class="row contenedor_colegios" id="contenedor_colegios" style="">
            {colegios}
              <div class="col-xs-12 col-sm-6 col-lg-4 p-4 colegios-item" >
                <div class="colegio link p-2" slug="{base_url}{slug}">
                <!-- nueva o clasicos -->
                  <div class="imagen-colegio" style="background-image: url('{base_url}images/web/front-colegios.png'); height: 200px;z-index: 1;position: relative;margin-bottom: -200px;"></div>
                  <div class="imagen-colegio" style="background-image: url('{card_image}'); height: 200px;"></div>
                  <div class="info-colegio bold mt-2">
                    {title} <small>{ano}</small>
                  </div>
                  <div class="info-colegio-s">
                    {city}
                  </div>
                </div>
              </div>
            {/colegios}
          </div>

      </div>
    </section>

</div>
