<!-- COMPUTER -->

<input type="hidden" id="idnumber" value="{idnumber}">
<input type="hidden" id="base_url" name="base_url" value="{base_url}">
<div class="contenedor computer">
  <a target="_blank" href="https://api.whatsapp.com/send?phone=5491158352669"><button class="botonF1 sinborde">
      <i class="fab fa-whatsapp fa-lg"></i>
    </button></a>
</div>
<div class="contenedor mobile">
  <a target="_blank" href="https://api.whatsapp.com/send?phone=5491158352669"><button class="botonF1-mobile sinborde">
      <i class="fab fa-whatsapp"></i>
    </button></a>
</div>



<nav class="navbar navbar-expand-lg navbar-dark fixed-top mainNav computer " id="mainNav" style="z-index: 5!important;">
  <div class="container-fluid align-items-center">

    <a class="js-scroll-trigger navbar-brand  nav-inverted" href="{base_url}" style="margin-left:20vw;">
      <img data-pin-nopin="false" src="{base_url}images/web/follow-me-logo-blue.png" height="60">
    </a>


        <div class="collapse navbar-collapse align-items-center mr-4" style="padding-right:15vw;">
          <ul class="navbar-nav text-uppercase ml-auto  nav-inverted">
            <a class="nav-link p-0" href="https://www.facebook.com/followmegrads" target="_blank">
              <img data-pin-nopin="false" class="icon-redes ml-2" src="{base_url}images/web/facebook.png">
            </a>
            <a class="nav-link p-0" href="https://www.instagram.com/Followmebuzosycamperas/" target="_blank">
              <img data-pin-nopin="false" class="icon-redes ml-2" src="{base_url}images/web/instagram.png">
            </a>
            {if {login} == 1}
            <a class="nav-link p-0" href="{base_url}user/logout">
              <img data-pin-nopin="false" class="icon-redes ml-2" src="{base_url}images/web/out.png">
            </a>
            {else}
            <a class="nav-link p-0" href="{base_url}user/login">
              <img data-pin-nopin="false" class="icon-redes ml-2" src="{base_url}images/web/userg.png">
            </a>
            {/if}
            <!-- <a class="nav-link p-0" href="" target="_blank"><img class="icon-redes ml-2" src="{base_url}images/web/pinterest.png"></a> -->
            <!-- <a class="nav-link p-0" href=""><img class="icon-redes ml-2" src="{base_url}images/web/tiktok.png"></a> -->
          </ul>

          <div id="nav-icon" class="modal-demo" style="margin-left:.5rem;margin-top:.3rem;">
            <div class="dot-nav"></div>
            <span></span>
            <span></span>
          </div>
        </div>


    </div>
  </nav>


<nav class="navbar navbar-expand-lg navbar-dark fixed-top mainNav mobile" id="mainNav">
  <div class="container align-items-center nav-inverted" style="z-index: 5!important;">
    <a class="js-scroll-trigger navbar-brand col-10 pr-0" href="{base_url}" style="margin-right: 0;">
      <img data-pin-nopin="false" src="{base_url}images/web/follow-me-logo-blue.png" height="50">
    </a>
    <div class="collapse navbar-collapse justify-content-left ml-auto col-2 pl-0">
      <div id="nav-icon-m" class="modal-demo">
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
</nav>
{div_separator}