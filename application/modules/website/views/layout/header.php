<!DOCTYPE html>
<html lang="es_AR">
<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

      <title>Buzos de egresados - Follow Me</title>

      <link rel="stylesheet" href="{base_url}jscript/bootstrap4/css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
      <link rel="icon" type="image/png" href="{base_url}images/web/favicon.png">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta property="og:site_name" content="Followme"/>
      <meta property="og:title" content="{title}"/>
      <meta property="og:description" content="{content}"/>
      <meta property="og:url" content="{base_url}{slug}"/>
      <meta property="og:image" content="{image}"/>
      <meta property="og:image:width" content="1200"/>
      <meta property="og:image:height" content="627"/>
      <meta property="og:image:alt" content="{title}"/>
      <meta name="format-detection" content="telephone=no">
      
      <meta property="og:locale" content="es_AR" />

       <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <!-- Custom fonts for this template -->
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700,800&display=swap" rel="stylesheet">
      <link href="{base_url}website/assets/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

      <link rel="stylesheet" href="{base_url}website/assets/fullPage/dist/fullpage.css">
      <link rel="stylesheet" type="text/css" href="{module_url}assets/css/all.css">
  <!-- Custom styles for this template -->
      <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/agency.css">
      <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/main.css">
      <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/breakpoints.css">
      <link rel="stylesheet" href="{base_url}jscript/WOW-master/css/libs/animate.css">
      <link rel="stylesheet" href="{base_url}website/assets/css/ap-fullscreen-modal.css">
      <link rel="stylesheet" href="{base_url}website/assets/slick/slick.css">
      <link rel="stylesheet" href="{base_url}website/assets/slick/slick-theme.css">
      <link rel="stylesheet" href="{base_url}website/assets/popup-master/dist/magnific-popup.css">
<!-- Global site tag (gtag.js) - Google Analytics -->
      <script src="{base_url}jscript/js/vendor/jquery/jquery.min.js"></script>
      <script src="{base_url}jscript/bootstrap4/js/bootstrap.bundle.min.js"></script>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-59145010-4"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-59145010-4');
      </script>
      <style type="text/css">
        html, body {
          margin: 0;
          padding: 0;
        }
        * {
          box-sizing: border-box;
        }
        .slider {
            width: 96%;
            margin: 0 auto;
        }
        .slick-slide {
          margin: 0px 20px;
        }
        .slick-slide img {
          width: 100%;
        }
        .slick-prev:before,
        .slick-next:before {
          color: black;
        }
        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }
        .slick-active {
          opacity: .5;
        }
        .slick-current {
          opacity: 1;
        }
      </style>
        <script src="{base_url}website/assets/slick/slick.min.js"></script>
        <script type="text/javascript" src="{base_url}website/assets/js/masonry.js"></script>
</head>


<body id="page-top" style="font-family:'Lato'!important;overflow-x: hidden;" >

  <input type="hidden" value="{base_url}" id="base_url">
