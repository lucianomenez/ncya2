<section class="page__title">
	<div class="container ">
		<div class="row" >
			<div class="col-2 container-lines">
			  <div class="lines-container">
		        <div class="yellow line"></div>
		        <div class="green line"></div>
		        <div class="lightblue line"></div>
		        <div class="pink line"></div>
		      </div>
			</div>
			<div class="col page-line">
			        <h1>{title}</h1>
			        <p><a href="{base_url}">Ir al inicio</a></p>
			</div>
	    </div>
  </div>
</section>
