<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_site extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_notas(){
        $this->db->switch_db('cms_ncya');

        $query = array('post_type'=> 'notas',
                      'post_status' => 'published');

        $this->db->where($query);
        $this->db->limit(6);
        $result = $this->db->get('container.pages')->result_array();
        return $result;
    }

    function insert_file($url, $id){
        $this->db->switch_db('cms_ncya');
        $query['_id'] =new MongoId($id);
        $this->db->where($query);
        $result = $this->db->get('container.pages')->result_array();
        $new_data = $result[0];
        $new_data['card_img'] = $url['uploadUrl'];
        $this->db->where($query);
        $this->db->update('container.pages', $new_data);
        return;

    }

    function delete_documento($id){
        $query['_id'] =new MongoId($id);
        $this->db->where($query);
        $this->db->delete('container.documentos');
        return;
    }


    function insert_documento($url){
        $this->db->insert('container.documentos', $url);
        return;
    }





}
