<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */
class Site extends MX_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('user');
      $this->load->model('Model_site');


			$this->load->config('config');
      $this->load->library('parser');

			//---base variables
			$this->base_url = base_url();
			$this->module_url = base_url() . $this->router->fetch_module() . '/';
			$this->lang->load('user/profile', $this->config->item('language'));
			$this->idu = (double) $this->session->userdata('iduser');
		  $this->user = $this->user->get_user($this->idu);

      error_reporting(E_ERROR | E_PARSE);

		}

    function index (){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $data['notas'] = $this->Model_site->get_notas();
      foreach ($data['notas'] as &$nota){
        if ((!isset($nota['card_img'])) or ($nota['card_img'] == "") ){
          $nota['card_img'] = $this->module_url .'assets/img/portfolio/item'.rand(1,6).'.jpg';
        }else{
          $nota['card_img'] = $this->base_url.$nota['card_img'];
        }
      }
      $this->parser->parse('index', $data);
    }

      private function main($page)
      {
        echo $this->parser->parse('main', $page);
      }

      private function slider($page)
      {
        echo $this->parser->parse('main', $page);
      }


      private function header($vars)
      {
        echo $this->parser->parse('header', $vars);
      }


      private function footer($vars)
      {
        echo $this->parser->parse('footer', $vars, false, false);
      }

      private function end($vars)
      {
        echo $this->parser->parse('end', $vars, false, false);
      }

      private function navbar($vars)
      {
        $vars['base_url'] = $this->base_url;
        $vars['module_url'] = $this->module_url;
        echo $this->parser->parse('navbar', $vars);
      }

      public function notas(){

        echo "hola";

      }


      function enviar_consulta(){
            $data = $this->input->post();
            $this->load->module('php_mailer/Php_mailer');
            $data=$this->input->post();

              $body= "<h3>Nombre:</h3> ".$data['name'].'<br>'.
                      "<b>Email:</b> ".$data['email'].'<br><br><br>'.
                      "<b><u>CONSULTA:</u></b> ".$data['message'].'<br><br>'.
                       "<i>" .nl2br($data['consulta'])."</i>";
              $msg=array(
                                      'subject'=>$data['subject'],
                                      'body'=>$body,
                                      'name'=>$data['name'],
                                      'from'=>$data['email'],
                                      'debug'=>true,
                                      );


             $this->php_mailer->send_mail($msg);
      }


}
