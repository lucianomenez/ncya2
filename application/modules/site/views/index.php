<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ESTUDIO JURIDICO - Negri, Curzi & Asociados</title>
		<!-- Meta Description -->
        <meta name="description" content="Estudio Jurídico - Negri Curzi y Asociados">
        <meta name="keywords" content="derecho penal economico estudio juridico capital federal">
        <meta name="author" content="Luciano Menez">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='{module_url}assets/css/google_fonts.css' rel='stylesheet' type='text/css'>
        <link rel="icon" type="image/png" href="{base_url}site/assets/img/favicon.ico"/>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{module_url}assets/css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{module_url}assets/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{module_url}assets/css/main.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{module_url}assets/js/modernizr-2.6.2.min.js"></script>

    </head>

    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
                    </button>
					<!-- /responsive nav button -->

					<!-- logo -->
					<!-- <h1 class="navbar-brand"> -->
						<img class="logo" src="{module_url}assets/img/negri250.png">
					<!-- </h1> -->
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="#body">Inicio</a></li>
                        <li><a href="{base_url}estudio" class="external">El estudio</a></li>
                        <li><a href="{base_url}servicios" class="external" >Servicios</a></li>
                        <li><a href="{base_url}profesionales" class="external">Profesionales</a></li>
                        <li><a href="{base_url}notas/indice" class="external">Publicaciones</a></li>
                        <li><a href="#contact">Contacto</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->

            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->

		<main class="site-content" role="main">

        <!--
        Home Slider
        ==================================== -->

		<section id="home-slider">
            <div id="slider" class="sl-slider-wrapper">

				<div class="sl-slider">

					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

						<div class="bg-img bg-img-1"></div>

						<div class="slide-caption">
                            <div class="caption-content">
                                <h2 class="animated fadeInDown">Negri, Curzi & Asociados</h2>
                                <span><i>"Una cosa no es justa por el hecho de ser ley. Debe ser ley porque es justa."</i><p style="font-size: 13px"><i>-Montesquieu-</i></p></span>
                                <a href="#contact" class="btn btn-blue btn-effect">Contacto</a>
                            </div>
                        </div>

					</div>

					<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">

						<div class="bg-img bg-img-2">
						<div class="slide-caption">
                            <div class="caption-content">
                                <h2>Negri, Curzi & Asociados</h2>
                                <span class="animated fadeInDown"><i>"Cuando hay tormenta...el águila vuela más alto"</i><p style="font-size: 13px"><i>-M Gandhi-</i></p></span>
                                <a href="{base_url}servicios" class="btn btn-blue btn-effect">Especialidad</a>
                            </div>
                        </div>
                        </div>

					</div>

					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">

						<div class="bg-img bg-img-3"></div>
						<div class="slide-caption">
                            <div class="caption-content">
                                <h2>Negri, Curzi & Asociados</h2>
                                <span>El Derecho Penal Económico requiere "trajes a medida".<br/> Es una rama del derecho absolutamente artesanal.<br/></span>
                                <a href="{base_url}estudio" class="btn btn-blue btn-effect">El Estudio</a>
                            </div>
                        </div>

					</div>

				</div><!-- /sl-slider -->

                <!--
                <nav id="nav-arrows" class="nav-arrows">
                    <span class="nav-arrow-prev">Previous</span>
                    <span class="nav-arrow-next">Next</span>
                </nav>
                -->

                <nav id="nav-arrows" class="nav-arrows hidden-xs hidden-sm visible-md visible-lg">
                    <a href="javascript:;" class="sl-prev">
                        <i class="fa fa-angle-left fa-3x"></i>
                    </a>
                    <a href="javascript:;" class="sl-next">
                        <i class="fa fa-angle-right fa-3x"></i>
                    </a>
                </nav>


				<nav id="nav-dots" class="nav-dots visible-xs visible-sm hidden-md hidden-lg">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
				</nav>

			</div><!-- /slider-wrapper -->
		</section>

        <!--
        End Home SliderEnd
        ==================================== -->

			<!-- about section -->
			<section id="about" >
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 wow animated fadeInRight">
							<div class="welcome-block">
								<h3 id="about">Bienvenidos</h3>
						     	 <div class="message-body texto-negri">
						       		<p style="font-size:22px;">Sólo sobreviven en Argentina los que prestando servicios generan confianza. La confianza y la discreción son elementos basales del buen servicio. Avalan lo expuesto casi 50 años de experiencia y dedicación a la atención plena y personalizada de nuestros diversos clientes (entre ellos destacados profesionales, empresas nacionales y multinacionales, instituciones financieras y funcionarios de los tres poderes del Estado), que nos han honrado requiriendo nuestro asesoramiento.</p>
						     	 </div>
						       	<!-- <a target="_blank" href="areas.html" class="btn btn-border btn-effect pull-right">Áreas de práctica</a> -->
						    </div>
						</div>
					</div>
				</div>
			</section>
			<!-- end about section -->




			<!-- Testimonial section -->
			<section id="testimonials" class="parallax">
				<div class="overlay">
					<div class="container">
						<div class="row">

							<div class="sec-title text-center white wow animated fadeInDown">
								<h2>Profesionales</h2>
							</div>

							<div id="testimonial" class=" wow animated fadeInUp">
								<div class="testimonial-item text-center">
									<img src="{module_url}assets/img/member-1.jpg" alt="Our Clients">
									<div class="clearfix">
										<a id="link-persona" href="{base_url}profesionales"><span>Dr. Carlos María Negri</span></a>
										<p>Abogado egresado con diploma de honor de la Universidad de Buenos Aires.  <br>Especialización en Derecho Penal Económico.</p>
									</div>
								</div>
								<div class="testimonial-item text-center">
									<img src="{module_url}assets/img/member-2.jpg" alt="Our Clients">
									<div class="clearfix">
										<a id="link-persona" href="{base_url}profesionales"><span>Dr. Sergio Fernando Curzi</span></a>
										<p>Abogado egresado de la Universidad de Buenos Aires. <br>Especialista en Derecho Penal y Ciencias Penales</p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
			<!-- end Testimonial section -->



			<!-- portfolio section -->
			<section id="portfolio">
				<div class="container">
					<div class="row">

						<div class="sec-title text-center wow animated fadeInDown">
							<h2>Publicaciones recientes</h2>
							<p></p>
						</div>


						<ul class="project-wrapper wow animated fadeInUp">
              {notas}
							<li class="portfolio-item">
								<a href="{base_url}{slug}"><img src="{card_img}" class="img-responsive" alt="">
								<figcaption class="mask">
									<h3>{title}</h3>
									<p>{bajada}</p>
								</figcaption>
							</li>
              {/notas}
						</ul>

					</div>
				</div>
			</section>
			<!-- end portfolio section -->


			<!-- Social section -->
			<section id="social" class="parallax">
				<div class="overlay">
					<div class="container">
						<div class="row">
							<div class="sec-title text-center white wow animated fadeInDown">
								<h2>Seguinos</h2>
								<div class="message-body">Nuestras Redes</div>
							</div>

                <span>
								<h4 class="wow animated zoomIn"><a href="https://www.facebook.com/pages/Estudio-Jur%C3%ADdico-Negri-y-Asociados/700824410043530"><i class="fa fa-facebook fa-2x" style="color : #ffffff"></i></a></h4>
                <h4 class="wow animated zoomIn"><a href="https://www.linkedin.com/company/negri-curzi-y-asociados"><i class="fa fa-linkedin fa-2x" style="color : #ffffff"></i></a></h4>

              </span>

						</div>
					</div>
				</div>
			</section>
			<!-- end Social section -->

			<!-- Contact section -->
			<section id="contact" >
				<div class="container">
					<div class="row">

						<div class="sec-title text-center wow animated fadeInDown">
							<h2>Contacto</h2>
							<div class="message-body">No dude en consultar</div>
						</div>


						<div class="col-md-7 contact-form wow animated fadeInLeft">
							<form action="{module_url}enviar_consulta" method="post">
								<div class="input-field">
									<input type="text" name="name" required class="form-control" placeholder="Nombre">
								</div>
								<div class="input-field">
									<input type="email" name="email" required class="form-control" placeholder="E-mail">
								</div>
								<div class="input-field">
									<input type="text" name="subject" required class="form-control" placeholder="Asunto">
								</div>
								<div class="input-field">
									<textarea name="message" class="form-control" required placeholder="Mensaje"></textarea>
								</div>
						       	<button type="submit" id="submit" class="btn btn-blue btn-effect">Enviar</button>
							</form>
						</div>

						<div class="col-md-5 wow animated fadeInRight">
							<address class="contact-details">
								<h3>Contáctenos</h3>
								<p><i class="fa fa-pencil"></i>Av. Pte. Roque Sáenz Peña 1160 - 6º piso.<span>C1035AAT</span> <span>CABA, Buenos Aires</span><span>Argentina</span></p><br>
								<p><i class="fa fa-phone"></i>(5411) 4382-5325 / 7103 / 2970 | 4383-7473 </p>
								<p><i class="fa fa-envelope"></i>Fax (5411) 4382-2970</p>
								<a target="_blank" href="https://api.whatsapp.com/send?phone=5491139444243"><p><i class="fab fa-whatsapp"></i>+549 11 3944 4243</p></a>

							</address>
						</div>

					</div>
				</div>
			</section>
			<!-- end Contact section -->

			<section id="google-map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.0604206494613!2d-58.384574372530764!3d-34.6026336308781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bccacf5434f9a1%3A0xa701aafa03495229!2sAv.%20Pres.%20Roque%20S%C3%A1enz%20Pe%C3%B1a%201160%2C%20C1048%20CABA!5e0!3m2!1ses!2sar!4v1590557213288!5m2!1ses!2sar" width="1280" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</section>

		</main>

		<footer id="footer">
			<div class="container">
				<div class="row text-center">
					<div class="footer-content">

						<!-- <form action="#" method="post" class="subscribe-form wow animated fadeInUp">
							<div class="input-field">
								<input type="email" class="subscribe form-control" placeholder="Enter Your Email...">
								<button type="submit" class="submit-icon">
									<i class="fa fa-paper-plane fa-lg"></i>
								</button>
							</div>
						</form>
						<div class="footer-social">
							<ul>
								<li class="wow animated zoomIn"><a href="#"><i class="fa fa-thumbs-up fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-skype fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="0.9s"><a href="#"><i class="fa fa-dribbble fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="1.2s"><a href="#"><i class="fa fa-youtube fa-3x"></i></a></li>
							</ul>
						</div> -->

						<p>Copyright &copy; 2020 - Estudio Negri, Curzi y Asociados</p>
					</div>
				</div>
			</div>
		</footer>

		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="{module_url}assets/js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="{module_url}assets/js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="{module_url}assets/js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="{module_url}assets/js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
		<!-- Owl Carousel -->
        <script src="{module_url}assets/js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="{module_url}assets/js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="{module_url}assets/js/jquery.slitslider.js"></script>
        <script src="{module_url}assets/js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="{module_url}assets/js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="{module_url}assets/js/main.js"></script>
    </body>
</html>
