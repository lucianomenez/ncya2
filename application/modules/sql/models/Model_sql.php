<?php

class Model_sql extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->idu = (int) $this->session->userdata('iduser');
        $this->load->library('cimongo/cimongo');
        $this->db = $this->cimongo;
        $this->container="container.calendar";
        $this->load->config('calendar/config');
    }
    
    //=== Entrega eventos entre dos fechas
    
    function get_events($myquery){
        
        $myuser=$this->user->get_user($this->idu);       
        $groups=(is_array($myuser->group))?($myuser->group):(array());     
        if (!isset($myquery['start']) || !isset($myquery['end'])) 
        	die("Please provide a date range.");
        
        $query=array(
            'start'=>array('$gte'=>$myquery['start']),
            'end'=>array('$lte'=>$myquery['end']),
            '$or'=>array(array('idu'=>$this->idu),array('group'=>array('$in'=>$groups)))
        );
        
        //=== hide groups 
        $groups=array();
        if(isset($myquery['exclude'])){
            // New setting - save
            while($mygroup=array_pop($myquery['exclude']['hide_groups']))
            if(is_numeric($mygroup)){
                $groups[]=(int)$mygroup;
            }
            $query['group']=array('$nin'=>$groups);
        }
        
        // Save exclude config
        $query_groups=array('class'=>'config','uid'=>$this->idu);
        $this->db->where($query_groups);
        $data['exclude']=$groups;
        $this->db->update($this->container,$data);
            


        $fields=array('title','start','end','tags','allDay','idu','color','group');
        //$query=array('start'=>array('$gte'=>'2015-06-08','$lte'=>'2015-09-20'));

        //$this->db->debug=true;
        $this->db->where($query,true);
        $this->db->select($fields);
        // $this->db->order_by($sort);
        $rs = $this->db->get($this->container)->result_array();

        // colors list
        
        $myconfig=$this->get_user_config();
        $colors=(isset($myconfig['colors']))?($myconfig['colors']):(array());

        //--- events loop
        $rs2=array();
        //$this->config->item('color_events'); //default
        
        foreach($rs as $k=>$evento ){
             $evento['_id']=(string)$evento['_id'];
             $gcolor=null;

             $default_group_color=$this->config->item('color_group_events');
                // group events 
                if( count($evento['group']) ){
                    //  var_dump($evento['group']);
                     $gcolor=$default_group_color;
                    // has group  - use group color
                    foreach($evento['group'] as $groupid){
                        if(isset($colors[$groupid])){
                            $gcolor=$colors[$groupid];
                            break;
                        }
                    }
                }

                if(isset($gcolor)){
                    // group event - 
                    $evento['color']=$gcolor;
                }

             $rs2[]=$evento;
        }
    
        return $rs2;

    }
    

    
    

}
