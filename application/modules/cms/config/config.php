<?php
/*
 * Brand string for dashboard
 */
$config['brand']='<img class="{base_url}images/png/logo.png">';
$config['default_cms']='cms/json/lite.json';
$config['gravatar']=false; //Support gravatar.com
$config['image_library'] = 'gd2';
$config['library_path'] = '/Applications/meanstack-3.6.3-2/php/include/php/ext/';
$config['presets'] = array(
        array('name' => 'thumbnail', 'operation' => 'resize-and-crop', 'dimensions' => array('width' => '150', 'height' => '150')),
        array('name' => 'large', 'operation' => 'resize', 'dimensions' => array('max_width' => '2000', 'max_height' => '2000')),
        array('name' => 'medium', 'operation' => 'resize', 'dimensions' => array('max_width' => '900', 'max_height' => '900')),
        array('name' => 'small', 'operation' => 'resize', 'dimensions' => array('max_width' => '450', 'max_height' => '450'))
);