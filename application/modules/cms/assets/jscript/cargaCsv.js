$(document).ready(function(){

  $(document).on({
    click: function(e) {
       var link = ($(this).attr('data-url'));


       var copyText =   $(this).parent().find('input')


       /* Select the text field */
       copyText.select();

       /* Copy the text inside the text field */
       document.execCommand("copy");

       /* Alert the copied text */
       alert("Link Copiado al portapapeles!");
    }
  }, '.copy_link');


    $(document).on({
      change: function(e) {
        e.preventDefault();

        let item=$(this).attr('data-input');
        let formData = new FormData();
        let file=$('#fileCsv')[0].files[0];
        formData.append("file", file);

        $.ajax({
              url: globals['base_url'] + 'cms/carga/process_documento',
              type: "POST",
              processData: false,
               contentType: false,
              data:formData
          }).done(function(response) {
           location.reload();
          })
      }
    }, '#fileCsv');
})
