<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include 'Node.php';

class Notas extends Node
{
    protected $node_type = 'notas';
    protected $card = true;
}
