<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Post
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 * @date    May 7, 2020
 */


include 'Node.php';

class Carga extends Node
{
    protected $node_type = 'carga';
    protected $card = false;

    // function process_documento(){
    //   $this->load->model('cms/Model_node');
    //   $this->load->library('cimongo/Cimongo', '', 'db');
    //   $this->config->load('cimongo');
    //
    //
    //   var_dump($_FILES);
    //     exit;
    //
    //
    //
    //     $tmpName = $_FILES['file']['tmp_name'];
    //     $data = array_map("str_getcsv", file($tmpName));
    //     unset($data[0]);
    //     $cant=0;
    //     $array_completo=array();
    //     foreach ($data as $item){
    //         $reg['tipo_file'] = 'items_catalogo';
    //         $reg['post_type'] = 'items_catalogo';
    //         $valueSlug=$this->slugify($reg['title']);
    //         $reg['slug'] = $valueSlug;
    //         $reg['title'] = $item[0];
    //         $reg['content'] = $item[1];
    //         $reg['enlace'] = $item[2];
    //
    //         $organismos= explode("/", $item[3]);
    //         if (is_array($organismos)) {
    //
    //         }
    //
    //         $destinatarios= explode("/", $item[4]);
    //         if (is_array($destinatarios)) {
    //           foreach ($destinatarios as $key_destinatario => $value_destinatario) {
    //             if ($value_destinatario!='') {
    //               $valueSlug=$this->slugify($value_destinatario);
    //               $array_completo[]=array("value"=>$valueSlug,"text"=>$value_destinatario);
    //             }
    //
    //           }
    //             $container='container.destinatarios';
    //             $this->Model_node->update_option($container,$array_completo);
    //                unset($array_completo['_id']);
    //             $reg['destinatarios']=$array_completo;
    //         }
    //         unset($array_completo);
    //
    //         $categorias= explode("/", $item[5]);
    //         if (is_array($categorias)) {
    //             foreach ($categorias as $key_categoria => $value_categoria) {
    //               if ($value_categoria!='') {
    //                 $valueSlug=$this->slugify($value_categoria);
    //                 $array_completo[]=array("value"=>$valueSlug,"text"=>$value_categoria);
    //               }
    //
    //             }
    //             $container='container.categorias';
    //             $this->Model_node->update_option($container,$array_completo);
    //                unset($array_completo['_id']);
    //             $reg['categorias']=$array_completo;
    //         }
    //         unset($array_completo);
    //
    //         $etiquetas= explode("/", $item[6]);
    //         if (is_array($etiquetas)) {
    //             foreach ($etiquetas as $key_etiqueta=> $value_etiqueta) {
    //               if ($value_etiqueta!='') {
    //                 $valueSlug=$this->slugify($value_etiqueta);
    //                 $array_completo[]=array("value"=>$valueSlug,"text"=>$value_etiqueta);
    //               }
    //
    //             }
    //             $container='container.tag';
    //             $this->Model_node->update_option($container,$array_completo);
    //             unset($array_completo['_id']);
    //             $reg['tag']=$array_completo;
    //         }
    //         unset($array_completo);
    //         $container = 'container.pages';
    //         $this->db->switch_db($this->config->item('cmsdb'));
    //         $query = array('_id' => new MongoId($data['_id']));
    //         $this->db->where($query);
    //         $this->db->update($container, $reg, array('upsert'=> true));
    //         unset($reg);
    //         $this->db->switch_db($this->config->item('db'));
    //         $cant++;
    //     }
    //
    //     echo $cant ." Registros insertados correctamente <br />";
    //
    // }


    function delete_item($id){
      $this->load->model('site/Model_site');
      $this->Model_site->delete_documento($id);
      redirect($this->module_url."carga");
    }



    function process_documento(){
      $this->load->model('site/Model_site');
      $data_post = $this->input->post();
      $data_file = $_FILES['file'];
      $segments = $this->uri->segment_array();
      $idmongo = $segments[3];

      $file_extension = pathinfo($data_file['name'], PATHINFO_EXTENSION);

      // Make dir
      if (!file_exists(FCPATH.'application/modules/files/assets/user_files/'.$this->idu)){
           @mkdir(FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/',0775,true);
      }

      // File configuration
      $config['file_name']            = $data_file['name'];
      $config['upload_path']          = FCPATH.'application/modules/files/assets/documentos/';
      $config['allowed_types']        = "gif|jpg|png|doc|DOC|docx|DOCX|jpeg|txt|TXT|JPG|PNG|JPEG|pdf|PDF|mp3|MP3|mp4|MP4|";
      $config['max_size']            = 10000;
      $config['overwrite']           = false;

      // Upload
      $this->load->library('upload', $config);

      // Set response data
      $response['uploadUrl'] = $this->base_url.'application/modules/files/assets/documentos/'.$data_file['name'];

      if ( ! $this->upload->do_upload('file'))
      {
          $error = array('error' => $this->upload->display_errors());
          var_dump($error, $config['upload_path']);
      }
      else
      {
          $file = ($this->upload->data());
          $data = array('upload_data' => $this->upload->data());
          $data['upload_path'] = $config['upload_path'];
          $file['uploadUrl'] = 'files/assets/documentos/';
          $response['filename'] = $data_file['name'];
          $this->Model_site->insert_documento($file);
      }
    }



    public function slugify($text)
      {

         $table = array(
            "à"=>'a',"ä"=>'a',"á"=>'a',"â"=>'a',"æ"=>'a',"å"=>'a',"ë"=>'e',"è"=>'e',"é"=>'e', "ê"=>'e',"î"=>'i',"ï"=>'i',"ì"=>'i',"í"=>'i',"ò"=>'o',"ó"=>'o',"ö"=>'o',"ô"=>'o',"ø"=>'o',"ù"=>'o',"ú"=>'u',"ü"=>'u',"û"=>'u',"ñ"=>'n',"ç"=>'c',"ß"=>'s',"ÿ"=>'y',"œ"=>'o',"ŕ"=>'r',"ś"=>'s',"ń"=>'n',"ṕ"=>'p',"ẃ"=>'w',"ǵ"=>'g',"ǹ"=>'n',"ḿ"=>'m',"ǘ"=>'u',"ẍ"=>'x',"ź"=>'z',"ḧ"=>'h',"·"=>'-',"/"=>'-',"_"=>'-',","=>'-',"=>"=>'-',";"=>'-'," "=>'-',"&"=>'-and-'
          );

        // lowercase
        $text = strtolower($text);
       // Trim - from start and end of text
        $text = trim($text);
        // Replace special characters using the hash map
        $text =strtr($text, $table);

        if (empty($text)) {
          return 'n-a';
        }

        return $text;
      }

}
