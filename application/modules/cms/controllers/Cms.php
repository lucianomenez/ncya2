
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 * @date    May 7, 2020
 */
class Cms extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('cms/config');

        $this->load->library('parser');
        $this->load->library('dashboard/ui');

        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('site/Model_site');
        $this->load->model('msg');
        $this->load->model('options');
        $this->load->model('cms/Model_node');
        $this->load->helper('form');
        $this->load->helper('cms/dynamic_builder');
        $this->load->helper('cms/block');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;

        error_reporting(E_ERROR | E_PARSE);


    }

    function index(){

        header("Location: ".   $this->base_url ."cms/pages");
        exit();

        $cms = ($this->session->userdata('json')) ? $this->session->userdata('json'):null;

        if($cms<>''){
            $this->CMS($cms);
        } else {
            $this->CMS($this->config->item('default_cms'));
        }
    }

    function Show($file, $debug = false) {
        //---only admins can debug
        $debug = ($this->user->isAdmin()) ? $debug : false;
        if (!is_file(APPPATH . "modules/cms/views/json/$file.json")) {
            // Whoops, we don't have a page for that!
            return null;
        } else {
            $myconfig = json_decode($this->load->view("cms/json/$file.json", '', true), true);
            if (isset($myconfig['private']) && $myconfig['private'] == true) {
                return;
            }
            $this->CMS("cms/json/$file.json", $debug);
        }
    }

    function get_listados(){
          $data=$this->input->post();

          $container= 'container.'.$data['item'];

          $result['data']= $this->options->get_options($container);

          if (!$result['data']) {
            $result='null';
          }
            echo json_encode($result);

    }

    function get_options_json($id){

          $container= 'container.'.$id;

          $result= $this->options->get_options($container);

          if (!$result) {
            $result='null';
          }

            echo json_encode($result);

    }

    function get_data(){
        $this->config->load('cimongo');
        $data=$this->input->post();
        //$args['post_status'] = 'published';
        $args['post_type'] =$data['post_type'];
        $args['_id']=new MongoId($data['_id']);
        $result['data']= $this->Model_node->get($args, 1)[0][$data['item']];
        if (!$result['data']) {
            $result='null';
        }
        echo json_encode($result);

    }



    function CMS($json = 'cms/json/cms.json',$debug = false,$extraData=null) {
        /* eval Group hooks first */
        $this->session->set_userdata('json', $json);
        $user = $this->user->get_user((int) $this->idu);
        $myconfig = $this->parse_config($json, $debug);

        $layout = ($myconfig['view'] <> '') ? $myconfig['view'] : 'layout';
        $customData = $myconfig;
        $customData['lang'] = $this->lang->language;
        $customData['alerts']=Modules::run('dashboard/alerts/get_my_alerts');
        $customData['brand'] = $this->config->item('brand');
        $customData['menu'] = $this->menu();
        $customData['avatar'] = Modules::run('user/profile/get_avatar'); //Avatar URL
        $customData['base_url'] = $this->base_url;

        $customData['module_url'] = $this->module_url;
        $customData['inbox_count'] = $this->msg->count_msgs($this->idu, 'inbox');
        $customData['config_panel'] =$this->parser->parse('_config_panel',  $customData['lang'], true, true);

        $customData['name'] = $user->name . ' ' . $user->lastname;
        $customData['email'] = $user->email;

        // Global JS
        $customData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url,
            'myidu' => $this->idu,
            'lang'=>$this->config->item('language')
        );


        // Toolbar
        $customData['toolbar_inbox'] = Modules::run('inbox/inbox/toolbar');

        $customData['new_toolbar_inbox'] = Modules::run('inbox/inbox/new_toolbar');

        if($extraData){
            $customData=$extraData+$customData;
        }
        $this->ui->compose($layout, $customData);
    }

    function loadDashboardVars($json = 'cms/json/cms.json',$debug = false,$extraData=null) {

        /* eval Group hooks first */
        $this->session->set_userdata('json', $json);
        $user = $this->user->get_user((int) $this->idu);
        $myconfig = $this->parse_config($json, $debug);

        $layout = ($myconfig['view'] <> '') ? $myconfig['view'] : 'layout';

        $customData = $myconfig;
        //$customData['lang'] = $this->lang->language;
        //$customData['alerts']=Modules::run('dashboard/alerts/get_my_alerts');
        $customData['brand'] = $this->config->item('brand');
        $customData['menu'] = $this->menu();
        $customData['avatar'] = Modules::run('user/profile/get_avatar'); //Avatar URL
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
        //$customData['inbox_count'] = $this->msg->count_msgs($this->idu, 'inbox');
        //$customData['config_panel'] =$this->parser->parse('_config_panel',  $customData['lang'], true, true);

        $customData['name'] = $user->name . ' ' . $user->lastname;
        $customData['email'] = $user->email;

        // Global JS
        $customData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url,
            'myidu' => $this->idu,
            'lang'=>$this->config->item('language')
        );


        // Toolbar
        //$customData['toolbar_inbox'] = Modules::run('inbox/inbox/toolbar');

        $customData['new_toolbar_inbox'] = Modules::run('inbox/inbox/new_toolbar');

        if($extraData){
            $customData=$extraData+$customData;
        }
        if($extraData){
            $customData=$extraData+$customData;
        }

        return $this->ui->compose_lte3($layout, $customData, true);
    }

    /**
     * Realiza los recortes (presets) definidos en cms/config.php sobre la imagen subida en $_FILES['file']
     * Una vez realizados aplicados los recortes, los sube a s3
     */


    function load_block($block_name, $block_count = 0, $block_data = ''){
        $block_pos = $block_count;
        $block_response = block_build_form($block_name, $block_data, $block_pos);
        header('Content-Type: application/json');
        echo json_encode($block_response);
    }

    function menu() {
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
        $customData['lang'] = $this->lang->language;
        $customData['is_admin']=$this->user->isAdmin();
        $customData['current_page'] = $this->base_url.$this->uri->segment(1).$this->uri->segment(2);

        $customData['menu_cms'] = array(
          array(
            'url' => $this->base_url.'cms/pages',
            'text' => 'Páginas',
            'icon' => 'file'
          ),
          array(
            'url' => $this->base_url.'cms/notas',
            'text' => "Notas",
            'icon' => 'bullseye'
         ),
          array(
          'url' => $this->base_url.'cms/carga',
            'text' => 'Carga de Documentos',
            'icon' => 'file'
          ),
          array(
          'url' => $this->base_url.'cms/catedras',
            'text' => 'La Cátedra',
            'icon' => 'university'
          )
        );

        return $this->parser->parse('cms/menu', $customData, true, true);
    }

    // ============ Parse JSON config
    function parse_config($file, $debug = false) {
        $myconfig = json_decode($this->load->view($file, '', true), true);
        $minwidth=2;

        //Root config

        foreach ($myconfig as $key => $value) {
            if ($key != 'zones')
                $return[$key] = $value;
        }


        $return['js']=array();
        $return['css']=array();
        $return['inlineJS']="";
        // CSS
        if(isset($myconfig['css'])){
            foreach($myconfig['css'] as $item){
                foreach($item as $k=>$v){
                    if(is_numeric($k))
                        $return['css'][]=$v;
                    else{
                        $return['css'][$k]=$v;
                    }
                }
            }
        }
        // JS
        if(isset($myconfig['js'])){
            foreach($myconfig['js'] as $item){
                foreach($item as $k=>$v){
                    if(is_numeric($k))
                        $return['js'][]=$v;
                    else{
                        $return['js'][$k]=$v;
                    }
                }
            }
        }
        return $return;
    }



    function upload_file(){
      $data_post = $this->input->post();
      $data_file = $_FILES['file'];
      $segments = $this->uri->segment_array();
      $idmongo = $segments[3];

      $file_extension = pathinfo($data_file['name'], PATHINFO_EXTENSION);

      // Make dir
      if (!file_exists(FCPATH.'application/modules/files/assets/user_files/'.$this->idu)){
           @mkdir(FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/',0775,true);
      }

      // File configuration
      $config['file_name']            = $data_file['name'];
      $config['upload_path']          = FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/';
      $config['allowed_types']        = "gif|jpg|png|doc|DOC|docx|DOCX|jpeg|txt|TXT|JPG|PNG|JPEG|pdf|PDF|mp3|MP3|mp4|MP4|";
      $config['max_size']            = 10000;
      $config['overwrite']           = false;

      // Upload
      $this->load->library('upload', $config);

      // Set response data
      $response['uploadUrl'] = $this->base_url.'application/modules/files/assets/user_files/'.$data_file['name'];

      if ( ! $this->upload->do_upload('file'))
      {
          $error = array('error' => $this->upload->display_errors());
          var_dump($error, $config['upload_path']);
      }
      else
      {
          $data = array('upload_data' => $this->upload->data());
          $data['upload_path'] = $config['upload_path'];
          $response['uploadUrl'] = 'files/assets/user_files/'.$this->idu.'/'.$data_file['name'];
          $this->Model_site->insert_file($response, $idmongo);
          echo json_encode($response);
      }
    }

}
