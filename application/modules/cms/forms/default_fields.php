<?php
return array(

    array(
        'type'  => 'input',
        'name'  => 'title',
        'label' => 'Título pestaña navegador',
        'class'  => 'title form-control',
    ),
    array(
        'type'  => 'input',
        'name'  => 'slug',
        'label' => 'Enlace permanente',
        'class'  => 'form-control',
    ),
    array(
        'type'  => 'hidden',
        'name'  => 'class'
    ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'legend',
      'text' => 'Información general',
      'class'  => '',
      'position' => 'sidebar'
    ),


    array(
        'type'  => 'datetime',
        'name'  => 'post_date',
        'label' => 'Fecha',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'hidden',
        'name'  => 'header_class',
        'position' => 'sidebar'
    ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'legend',
      'text' => 'Metadatos <br><small>(Información para las redes)</small>',
      'class'  => '',
      'position' => 'sidebar'
    ),
    array(
        'type'  => 'image_upload',
        'name'  => 'featured_image',
        'label' => 'Imagen destacada',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),

    array(
        'type'  => 'textarea',
        'name'  => 'meta_description',
        'label' => 'Meta Description',
        'class'  => 'form-control',
        'rows'  => 2,
        'position' => 'sidebar'
    )
);
