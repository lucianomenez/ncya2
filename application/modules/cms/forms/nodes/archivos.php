<?php
return array(

    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'name'  => 'tipo_file',
        'value' => 'notas',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
    array(
                'type'  => 'input',
                'name'  => 'title',
                'label' => 'Título',
                'class' => 'form-control'
            ),

    array(
                        'type'  => 'input',
                        'name'  => 'bajada',
                        'label' => 'Bajada',
                        'class' => 'form-control'
                    ),

 	array(
                'type'  => 'textarea',
                'name'  => 'content',
                'label' => 'Texto',
                'class'  => 'form-control load-js redactor',
                'data-load' => 'redactor'
            ),
    array(
                'type'  => 'input',
                'name'  => 'enlace',
                'label' => 'Enlace Externo',
                'class' => 'form-control'
            ),

);
