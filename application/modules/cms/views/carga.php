<?php
/*
 *  Header : CSS Load & some body
 *
 */
$this->load->view('_lte3_header.php')
?>

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        <form>
          <div class="form-group">
            <h4 for="fileCsv">Subir Documento de Word o Pdf</h4>
            <input type="file" class="form-control-file" id="fileCsv" accept=".pdf, .docx, .doc">
          </div>
            <button class="btn btn-verde mb-4 uploadFile" id="inputcsv" data-input="fileCsv"><i class="fas fa-plus-circle"></i> Enviar</button>
        </form>
      </div>

    </div>
    <!-- /.row -->
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h4 for="fileCsv">Listado de Documentos Subidos</h4>
        <div id="tabla_documentos">

          <table id="" class="datatables table table-bordered table-hover" style="background-color: #fff;">
              <thead>
                <tr>
                  <th style="text-align: center;">Nombre Archivo</th>
                  <th style="text-align: center;">OPERACIONES</th>
                  <th style="text-align: center;">Dirección</th>
                </tr>
              </thead>
              <tbody>
                {documentos}
                <tr class="node">
                  <td><a href="{base_url}{uploadUrl}{file_name}" style="width: 100%;display: inline-block;">{file_name}</a></td>
                  <td style="text-align: center;"><a href="{base_url}{uploadUrl}{file_name}" style='font-size: 17px;width: 25px;display: inline-block;'><i class="fas fa-eye"></i></a><a href="{base_url}cms/carga/delete_item/{_id}" class="node-delete" style='font-size: 17px;width: 25px;display: inline-block;'><i class='fas fa-trash-alt'></i></a></td>
                  <td style="text-align: center;"> <input type="text" readonly value="{base_url}{uploadUrl}{file_name}" id="myInput"> <button data-url="{base_url}{uploadUrl}{file_name}" class="copy_link"> Copiar</button><td>
                </tr>
                {/documentos}
              </tbody>
          </table>

        </div>

      </div>

    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /.content -->
<script src="{base_url}cms/assets/jscript/cargaCsv.js"></script>

{ignore}
<script>
  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
  }
</script>
{/ignore}
{js}
<?php
/*
 *  FOOTER
 *
 */
$this->load->view('_lte3_footer.php')

?>
