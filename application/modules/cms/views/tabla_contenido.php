<table id="" class="datatables table table-bordered table-hover" style="background-color: #fff;">
    <thead>
      <tr>
        <th style="text-align: center;">Nombre Archivo</th>
        <th style="text-align: center;">OPERACIONES</th>
      </tr>
    </thead>
    <tbody>
      {nodes}
      <tr class="node">
        <td><a href="{full_path}" style="width: 100%;display: inline-block;">{file_name}</a></td>
        <td style="text-align: center;"><a href="{base_url}{edit_url}{_id}" style="font-size: 17px;width: 25px;display: inline-block;"><i class="fas fa-edit" ></i></a> <a href="{base_url}{slug}" style="font-size: 17px;width: 25px;display: inline-block;"><i class="fas fa-eye"></i></a><a href="{base_url}cms/node/carga/delete/{_id}" class="node-delete" style="font-size: 17px;width: 25px;display: inline-block;"><i class="fas fa-trash-alt"></i></a></td>
      </tr>
      {/nodes}
    </tbody>
</table>
