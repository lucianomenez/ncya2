<?php
/*
 *  Header : CSS Load & some body
 *
 */
$this->load->view('_lte3_header.php')
?>

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <?php echo form_open($form_action, '', $id_hidden ); ?>
    <div class="row">

      <div class="col-lg-8">

        {form}

      </div>
      <div class="col-lg-4">

        {form_sidebar}

      </div>
      <!-- /.col-md-6 -->
    </div>

    <!-- /.row -->
  </div><!-- /.container-fluid -->
  <?php echo form_close(); ?>
</div>
<!-- /.content -->

<div id="dialog" style="display:none" title="Confirmation Required">
    ¿Está seguro que desea borrar este bloque?, esta acción no se puede deshacer.
</div>

<?php
/*
 *  FOOTER
 *
 */
$this->load->view('_lte3_footer.php')

?>
