<?php
if ( ! defined('BASEPATH')) exit('Access denied');

include_once 'Model_node.php';

class Model_carga extends Model_node {

    function get($params = array(), $limit = 25, $offset = 0){
        $params['post_type'] = 'carga';
        $result= parent::get($params, $limit);
        if (!$result) {
            $result=array();
        }
         return $result;
    }

    function save($data){
        $data['post_type'] = 'carga';
        return parent::save($data);
    }

    function get_documentos(){
          $this->db->switch_db($this->config->item('db'));
          $result = $this->db->get('container.documentos')->result_array();
          return $result;



    }

}
