<?php
if ( ! defined('BASEPATH')) exit('Access denied');

include_once 'Model_node.php';

class Model_notas extends Model_node{

	function get($params = array(), $limit = 40, $offset = 0 ){
      $params['post_type'] = 'notas';
	    $result= parent::get($params, $limit);
	    if (!$result) {
	    	$result=array();
	    }
	     return $result;
	}

	function save($data){
	    $data['post_type'] = 'notas';
	    return parent::save($data);
    }

}
