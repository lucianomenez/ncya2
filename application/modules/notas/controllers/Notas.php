<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */
class Notas extends MX_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('user');

			$this->load->config('config');
      $this->load->library('parser');
      $this->load->model('cms/Model_node');
      $this->load->model('Model_notas');

			//---base variables
			$this->base_url = base_url();
			$this->module_url = base_url() . $this->router->fetch_module() . '/';
			$this->lang->load('user/profile', $this->config->item('language'));
			$this->idu = (double) $this->session->userdata('iduser');
		  $this->user = $this->user->get_user($this->idu);

      error_reporting(E_ERROR | E_PARSE);

		}




function indice(){
  $data['base_url'] = $this->base_url;
  $data['module_url'] = $this->module_url;
  $params = array('slug' => $this->uri->uri_string);
  if ($page = $this->Model_node->get_one($params)) {
    $data['nota'] = $this->nota($page);
    $data['notas_indice'] = false;
    echo $this->parser->parse('index',$data, true, true);
  } else {
    // muestro 404
    $data['notas_indice'] = true;
    $data['notas_array'] = $this->Model_notas->get_notas();

    foreach ($data['notas_array'] as &$nota){
      if ((!isset($nota['card_img'])) or ($nota['card_img'] == "") ){
        $nota['card_img'] = $this->base_url .'site/assets/img/portfolio/item'.rand(1,6).'.jpg';
      }else{
        $nota['card_img'] = $this->base_url.$nota['card_img'];
      }
    }

    echo $this->parser->parse('index',$data, true, true);
  }
}


      private function notas($data)
      {
        $data['notas'] = $data;
        return $this->parser->parse('notas', $data);
      }

      private function nota($page)
      {
        return $this->parser->parse('nota', $page);
      }




}
