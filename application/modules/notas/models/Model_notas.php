<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_notas extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_notas(){
       $this->db->select(array('slug', 'title', 'bajada', 'post_type', 'card_img'));
        $this->db->switch_db('cms_ncya');
        $query = array('post_type'=> 'notas',
                        "post_status" => 'published');
        $this->db->where($query);
        $this->db->order_by(array ("post_date" => desc));        
        $result = $this->db->get('container.pages')->result_array();
        return $result;
    }






}
