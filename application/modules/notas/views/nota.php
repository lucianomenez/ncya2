<section>
  <div class="container" style="margin-top: 150px;">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        <div class="sec-title text-center wow animated fadeInDown">
          <h2>{title}</h2>
          <p></p>
        </div>

        <h3 class="subtitle">{bajada}</h3>
        <p>{content}</p>
      </div>
    </div>
  </div>
</section>
