
<!DOCTYPE html>

<html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ESTUDIO JURIDICO - Negri, Curzi & Asociados</title>
        <link rel="icon" type="image/png" href="{base_url}site/assets/img/favicon.ico"/>

		<!-- Meta Description -->
        <meta name="description" content="ESTUDIO JURIDICO - Negri, Curzi & Asociados">
        <meta name="keywords" content="Estudio Jurídico Derecho Penal Comercial Buenos Aires Trayectoria Curzi Negri">
        <meta name="author" content="Luciano Menez">

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='{base_url}site/assets/css/google_fonts.css' rel='stylesheet' type='text/css'>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{base_url}site/assets/css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{base_url}site/assets/css/main.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{base_url}site/assets/js/modernizr-2.6.2.min.js"></script>

    </head>

    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
          </button>
					<!-- /responsive nav button -->

					<!-- logo -->
					<!-- <h1 class="navbar-brand"> -->
						<img class="logo" src="{base_url}site/assets/img/negri250.png">
					<!-- </h1> -->
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="{base_url}" class="external">Inicio</a></li>
                        <li><a href="{base_url}estudio" class="external">El estudio</a></li>
                        <li><a href="{base_url}servicios" class="external" >Servicios</a></li>
                        <li><a href="{base_url}profesionales" class="external">Profesionales</a></li>
                        <li><a href="{base_url}notas/indice" class="external">Publicaciones</a></li>
                        <li><a href="{base_url}" class="external">Contacto</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->

            </div>
        </header>

        <section id="cab-interna2" class="parallax cab-interna">
            <div class="overlay">
              <div class="container">
                <div class="row">

                  <div class="sec-title-int text-center white wow animated fadeInDown">
                    <h2>La Cátedra</h2>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <main class="site-content" role="main">

            <section>

              <div class="overlay">
                <div class="container">
                  <div class="row col-md-12">

                  <a href="{module_url}indice/" class="btn btn-blue btn-effect">Índice</a>
                  <a href="{module_url}indice/libro-I" class="btn btn-blue btn-effect">Libro I</a>
                  <a href="{module_url}indice/libro-II" class="btn btn-blue btn-effect">Libro II</a>
                  <a href="{module_url}indice/libro-III" class="btn btn-blue btn-effect">Libro III</a>
                  <a href="{module_url}indice/libro-IV" class="btn btn-blue btn-effect">Libro IV</a>
                  <a href="{module_url}indice/archivos" class="btn btn-blue btn-effect">Archivos Adjuntos</a>


                </div>
              </div>
            </div>

          </section>



          <section>
            <div class="container" style="margin-top: 50px;">
              <div class="row">
                <div class="col-md-8 col-md-offset-2">

                  <div class="sec-title text-center wow animated fadeInDown">
                    <h2>{title}</h2>
                    <p></p>
                  </div>

                  <h3 class="subtitle">{bajada}</h3>
                  <p>{content}</p>
                </div>
              </div>
            </div>
          </section>

        </main>

        <script src="{base_url}site/assets/js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="{base_url}site/assets/js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="{base_url}site/assets/js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="{base_url}site/assets/js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
		<!-- Owl Carousel -->
        <script src="{base_url}site/assets/js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="{base_url}site/assets/js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="{base_url}site/assets/js/jquery.slitslider.js"></script>
        <script src="{base_url}site/assets/js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="{base_url}site/assets/js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="{base_url}site/assets/js/main.js"></script>
    </body>
</html>
