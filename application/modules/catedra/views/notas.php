<section id="portfolio">
  <div class="container">
    <div class="row">

      <div class="sec-title text-center wow animated fadeInDown">
        <h2>Índice de Notas</h2>
        <p></p>
      </div>


      <ul class="project-wrapper wow animated fadeInUp">
        {notas_array}
        <li class="portfolio-item">
          <a href="{base_url}{slug}"><img src="{module_url}assets/img/portfolio/item1.jpg" class="img-responsive" alt="">
          <figcaption class="mask">
            <h3>{title}</h3>
            <p>{bajada}</p>
          </figcaption>
        </li>
        {/notas_array}
      </ul>

    </div>
  </div>
</section>
