<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Perfil extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->library('ui');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->usuario = $this->user->get_user_array($this->idu);

    }

    // ============ HEADER DE CONTENT - TITULO Y breadcrumb
    function content_header() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('perfil/content_header', $data, true, true);
    }

    // ============ SCRIPTS DEL FOOTER
    function scripts() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('perfil/scripts', $data, true, true);
    }

    // ============ SIDEBAR GRIS
    function footer() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('perfil/footer', $data, true, true);
    }

    // ============ SIDEBAR GRIS
    function sidebar() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        $data['name']= $this->usuario['name'];
        $data['lastname']= $this->usuario['lastname'];
        echo $this->parser->parse('perfil/sidebar', $data, true, true);
    }

    // ============ NAVBAR HORIZONTAL DE ARRIBA
    function navbar() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('perfil/navbar', $data, true, true);
    }

    // ============ CONFIGURACION DE HEAD. CSS Y LIBRERIAS
    function head() {
        $data['title'] = 'Mapa de la Acción Estatal | Panel';
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('perfil/head', $data, true, true);
    }



}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
