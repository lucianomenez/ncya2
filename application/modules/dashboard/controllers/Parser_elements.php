<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parser_elements extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
    }

    function navbar_jquery_semantic() {
        $data['base_url'] = $this->base_url;
        echo $this->parser->parse('dashboard/_navbar_jquery_semantic', $data, true, true);
    }

    function footer_jquery_semantic() {
        $data['base_url'] = $this->base_url;
        echo $this->parser->parse('dashboard/_footer_jquery_semantic', $data, true, true);
    }
}