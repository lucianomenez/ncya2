<html>
  <head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>{title}</title>
    <link rel="shortcut icon" href="{base_url}jscript/semantic/images/favicon.png" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">    
    <script src="{base_url}jscript/librerias/jquery_3-5/jquery-3.5.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{base_url}jscript/frameworks/semantic_2-4/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/breakpoints.css">
    {custom_css}
  </head>
  <body class="site" style="/*background: #fefefe;*/">
