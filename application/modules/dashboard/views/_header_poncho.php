<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{title}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!--====== CSS BASE ===== -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&display=swap" rel="stylesheet">
        <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"  rel="stylesheet"> -->
        <link href="{base_url}dashboard/assets/css/all.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/semantic.css">
        <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/breakpoints.css">
        <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/main.css">
        <!-- <link href="{base_url}poncho/assets/css/poncho.min.css" rel="stylesheet">
        <link href="{base_url}poncho/assets/css/icono-arg.css" rel="stylesheet"> -->



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        {custom_css}
    </head>

    <div class="ui fluid container shadow computer mb2" style="background: #fff!important; padding:.5rem 1rem .5rem 1rem;">
      <div class="ui container">
        <div class="ui secondary menu">
          <img alt="Premio Nacional a la Calidad" src="http://pnc.argentina.gob.ar/img/logos/logo_pnc.png"/>
          <div class="right menu">
            <div class="ui dropdown item">Inscripción
                  <i class="dropdown icon"></i>
                  <div class="menu menu-inscripcion">
                    <a class="item" href="{base_url}inscripcion/organismo">Organismo</a>
                    <!-- <a class="item" href="">Evaluador</a>
                    <a class="item" href="">Juez</a> -->
                  </div>
            </div>
            <a class="item" href="{base_url}autoevaluacion/inscripcion">Autoevaluacion</a>
            <!-- <a class="item" href="{base_url}normativa">Normativa</a> -->
            <div class="ui dropdown item">{name}
                  <i class="dropdown icon"></i>
                  <div class="menu menu-inscripcion">
                    <a class="item" href="{base_url}panel">Panel</a>
                    <a class="item" href="{base_url}panel/mis_datos">Actualizar datos</a>
                    <a class="item btn_contrasena">Cambiar contraseña</a>
                    <a class="item" href="{base_url}user/logout">Cerrar Sesión</a>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ui fluid container shadow mobile mb2" style="background: #fff!important; padding:.5rem 1rem .5rem 1rem; margin-left:0!important; margin-right:0!important;">
      <div class="ui container">
        <div class="ui secondary menu">
          <img alt="Premio Nacional a la Calidad" src="http://pnc.argentina.gob.ar/img/logos/logo_pnc.png" style="width:80%!important;"/>
          <div class="right menu mt1">
            <div class="ui dropdown"><i class="fas fa-bars fa-2x"></i>
            <div class="menu menu-mobile p1">
              <h4 class="menu-hola">{name}</h4>
              <div class="ui divider ml1 mr1"></div>
              <a class="item mt1" href="{base_url}panel">Panel</a>
              <a class="item mt1" href="{base_url}inscripcion/organismo">Inscribirme como organismo</a>
              <a class="item mt1" href="{base_url}">Autoevaluacion</a>
              <a class="item mt1" href="{base_url}panel/mis_datos">Actualizar datos</a>
              <a class="item mt1 btn_contrasena">Cambiar contraseña</a>
              <a class="item mt1" href="{base_url}user/logout">Cerrar Sesión</a>
              <!-- <a class="item" href="{base_url}normativa">Normativa</a> -->

            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <!-- <nav class="navbar navbar-top navbar-default">
      <div class="container">
        <div class="navbar-header">
            <a href="{base_url}" class="navbar-brand" style="padding: 0px;">
                <img alt="Premio Nacional a la Calidad" src="http://pnc.argentina.gob.ar/img/logos/logo_pnc.png" />
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
              <li  class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">AUTOEVALUACIÓN
                  <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li class="sub-li"><a href="{base_url}autoevaluacion/inscripcion">Inscripción</a></li>
                      <li class="sub-li"><a  href="{base_url}autoevaluacion/acceso">Acceso</a></li>
                    </ul>
                     </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inscripciones
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li class="sub-li"><a href="{base_url}inscripcion/organismo/">Organismo</a></li>
                          <li class="sub-li"><a href="{base_url}inscripcion/evaluador">Evaluador</a></li>
                          <li class="sub-li"><a href="{base_url}inscripcion/juez">Juez</a></li>
                        </ul>
                    </li>

                <li><a href="{base_url}user/login">Ingresar</a></li>
                <li><a href="{base_url}user/register">Registrarse</a></li>

              </ul>
      </div>
    </nav> -->
