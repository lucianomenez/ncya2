<div class="ui fluid container mt3" style="background: #fff!important; padding:3rem 1rem 3rem 1rem; margin-left:0!important; margin-right:0!important;">
  <div class="ui container">
    <div class="ui stackable two column grid centered">
      <div class="column">
        <img class="computer" alt="Argentina.gob.ar - Presidencia de la Nación" src="http://pnc.argentina.gob.ar/img/logos/logo_presidencia.png"/>
        <img class="mobile" alt="Argentina.gob.ar - Presidencia de la Nación" style="width:100%!important" src="http://pnc.argentina.gob.ar/img/logos/logo_presidencia.png"/>
      </div>
     <div class="column">
       <div class="ui vertical text menu">
        <a class="item footer" href="{module_url}panel">Panel</a>
        <a class="item footer" href="http://pnc.argentina.gob.ar/normativa">Normativa</a>
       </div>
     </div>
    </div>
  </div>
</div>


<!-- ======== MODAL ======== -->
<div class="ui modal" id="cambiar_contrasena">
  <div class="close icon"><i class="fas fa-times"></i></div>
  <div class="header"><h3>Cambiar contraseña</h3></div>
  <div class="content">
    <div class="ui form">
      <div class="two fields">
        <div class="field">
          <label>Nueva contraseña</label>
          <input type="text" placeholder="Nueva contraseña">
        </div>
        <div class="field">
          <label>Confirmar contraseña</label>
          <input type="text" placeholder="Confirma contraseña">
        </div>
      </div>
        <div class="field">
          <button type="submit" class="ui button green">Guardar</button>
        </div>
      </div>
    </div>
  </div>
</div>

 <!-- ======== FOR PRINTING MSGS ======== -->
<!-- ________ FOR PRINTING MSGS ________ -->


<!-- JS Global -->
<script>
    //-----declare global vars
    var globals = {global_js};
</script>

<!-- JS custom -->
<script src="{base_url}dashboard/assets/jscript/jquery.min.js"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<script src="{base_url}dashboard/assets/jscript/semantic.min.js"></script>
<script src="{base_url}dashboard/assets/jscript/custom.js"></script>
<!-- JS inline -->
{js}
<script>
{ignore}
$(document).ready(function(){
  {inlineJS}
});
{/ignore}
</script>

</body>
</html>
