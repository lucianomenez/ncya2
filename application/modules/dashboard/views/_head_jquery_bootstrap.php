<!DOCTYPE html>
<html>
<head>
    <html lang="es_AR">
    <meta name="viewport" content="initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{title}</title>
    <link rel="shortcut icon" href="" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">    
    <script src="{base_url}jscript/librerias/jquery_3-5/jquery-3.5.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{base_url}jscript/frameworks/bootstrap-4.4.1/css/bootstrap.min.css"></link>
    <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/breakpoints.css">
    {custom_css}
</style>

    <meta name="title" content="{title}"/>
    <meta name="description" content="{meta_description}"/>
    <meta name="keywords" content="{meta_keywords}"/>

    <meta property="og:site_name" content="MAPA DE LA ACCIÓN ESTATAL"/>
    <meta property="og:title" content="{title}"/>
    <meta property="og:description" content="{meta_description}"/>
    <meta property="og:url" content="{base_url}{slug}"/>
    <meta property="og:image" content="{featured_image}"/>
    <meta property="og:locale" content="es_AR" />

</head>
<body>
<input type="hidden" value="{base_url}" id="base_url">
<input type="hidden" value="{slug}" id="type_page">

