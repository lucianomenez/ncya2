<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ESTUDIO JURIDICO - Negri, Curzi & Asociados</title>
        <link rel="icon" type="image/png" href="{base_url}site/assets/img/favicon.ico"/>

    <!-- Meta Description -->
        <meta name="description" content="Filosofía Estudio Jurídico Negri Curzi y Asociados">
        <meta name="keywords" content="Estudio Jurídico Curzi Negri Derecho Penal Economico">
        <meta name="author" content="Luciano Menez">

		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS
		================================================== -->

		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{base_url}site/assets/css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="{base_url}site/assets/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{base_url}site/assets/css/main.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{base_url}site/assets/js/modernizr-2.6.2.min.js"></script>

    </head>

    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
                    </button>
					<!-- /responsive nav button -->

					<!-- logo -->
					<img class="logo" src="{base_url}site/assets/img/negri250.png">
					<!-- /logo -->
                </div>

				<!-- main nav -->
          <nav class="collapse navbar-collapse navbar-right" role="navigation">
              <ul id="nav" class="nav navbar-nav">
								<li><a href="{base_url}" class="external">Inicio</a></li>
								<li><a href="{base_url}estudio" class="external">El estudio</a></li>
								<li><a href="{base_url}servicios" class="external">Servicios</a></li>
								<li><a href="{base_url}profesionales" class="external">Profesionales</a></li>
								<li><a href="{base_url}notas/indice" class="external">Publicaciones</a></li>
								<li><a href="{base_url}" class="external">Contacto</a></li>
              </ul>
          </nav>
				<!-- /main nav -->

            </div>
        </header>
<!-- about section -->
		<section id="cab-interna" class="parallax">
				<div class="overlay">
					<div class="container">
						<div class="row">

							<div class="sec-title-int text-center white wow animated fadeInDown">
								<h2>El Estudio</h2>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="areas">
				<div class="container">
					<div class="row">
            <div class="col-md-4 col-md-offset-1 wow animated fadeInLeft">
							<div class="recent-works">
                <div class="sec-title  wow animated fadeInDown">
    							<b><h3>Nuestra Filosofía</h3></b>
    						</div>

								<div id="works">
									<div class="work-item">
										<p>Fundado en el año 1971, el Estudio Negri, Curzi y Asociados considera que el respeto y la confianza constituyen los únicos parámetros en los que puede sustentarse un adecuado vínculo asesor-cliente.<br>
											<br>
										Dedicado al Derecho Penal y Penal Económico, esa patología del derecho comercial, sus socios tienen una larga experiencia en el ejercicio profesional y en la función pública.</p>
									</div>
									<div class="work-item">
										<p>Sus integrantes consideran que, para prestar adecuados servicios, resulta indispensable analizar los problemas con una visión interdisciplinaria, circular, que abarque los 360º.<br>
											<br>
										No basta el derecho penal económico, muchas veces resulta necesaria una visión civil, comercial, administrativa o laboral que complemente e integre la visión penal. Lo contrario, estiman, haría ver los conflictos en forma sesgada, no completa y por lo tanto parcial.</p>
									</div>
                  <br/>
									<div class="work-item">
										<p>Su principal activo es el conocimiento aplicado. Sus socios han aprendido a distinguir entre información, conocimiento y sabiduría. Sus principales enemigos son la ignorancia, la anomia y el odio. Solo trabajamos con los mejores y ello se plasma en su labor.<br>
											<br>
										Su primera prioridad es resguardar los intereses de sus representados a los que se les brinda una atención personalizada.</p>
									</div>
								</div>
							</div>
						</div>
            <div class="col-md-4 col-md-offset-2">
              <br/><br/><br/><br/>
              <img class="mt-4" src="{base_url}site/assets/img/estudio_demo.jpg">
            </div>



					</div>
          <br/>
				</div>
			</section>

<br/><br/>


      <footer id="footer">
  			<div class="container">
  				<div class="row text-center">
  					<div class="footer-content">

  						<!-- <form action="#" method="post" class="subscribe-form wow animated fadeInUp">
  							<div class="input-field">
  								<input type="email" class="subscribe form-control" placeholder="Enter Your Email...">
  								<button type="submit" class="submit-icon">
  									<i class="fa fa-paper-plane fa-lg"></i>
  								</button>
  							</div>
  						</form>
  						<div class="footer-social">
  							<ul>
  								<li class="wow animated zoomIn"><a href="#"><i class="fa fa-thumbs-up fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-skype fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="0.9s"><a href="#"><i class="fa fa-dribbble fa-3x"></i></a></li>
  								<li class="wow animated zoomIn" data-wow-delay="1.2s"><a href="#"><i class="fa fa-youtube fa-3x"></i></a></li>
  							</ul>
  						</div> -->

  						<p>Copyright &copy; 2020 - Estudio Negri, Curzi y Asociados</p>
  					</div>
  				</div>
  			</div>
  		</footer>
		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="{base_url}site/assets/js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="{base_url}site/assets/js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="{base_url}site/assets/js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="{base_url}site/assets/js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Owl Carousel -->
        <script src="{base_url}site/assets/js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="{base_url}site/assets/js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="{base_url}site/assets/js/jquery.slitslider.js"></script>
        <script src="{base_url}site/assets/js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="{base_url}site/assets/js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="{base_url}site/assets/js/main.js"></script>
    </body>
</html>
